using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameObjects;
using Zenject;

namespace Kudos.GameControllers
{
    public class PoolsManager
    {
        public Projectile TargetPrefab { get; private set; }
        public Transform TargetTransformGroup { get; private set; }

        private DiContainer _container;
        private Projectile.ProjectileFactory _factory;

        public List<Projectile.Pool> Pools { get; private set; }

        public PoolsManager(DiContainer container, Projectile.ProjectileFactory factory)
        {
            Pools = new List<Projectile.Pool>();
            _container = container;
            _factory = factory;
        }


        public Projectile.Pool InstantiatePool(Projectile prefab, int size, string transformGroup)
        {
            TargetPrefab = prefab;
            TargetTransformGroup = new GameObject(transformGroup).transform;
            var settings = new MemoryPoolSettings(
           // Initial size
           size,
           // Max size
           int.MaxValue,
           PoolExpandMethods.Disabled);
            // var pool = _container.Instantiate<MemoryPool<Projectile>>();
            var pool = _container.Instantiate<Projectile.Pool>(
                     new object[] { settings, _factory });
            Pools.Add(pool);
            return pool;
        }
    }
}
