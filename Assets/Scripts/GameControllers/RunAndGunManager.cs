using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Kudos.GameObjects;

namespace Kudos.GameControllers
{
    public interface IGameManager
    {
        void Rebuild();
        void StartGame();
        void EndGame();
        void CompleteGame();

        event Action OnRebuild;
        event Action OnStartGame;
        event Action OnEndGame;
        event Action OnCompleteGame;
    }
    public interface IManageable
    {
        void Rebuild();
        void StartExecution();
        void FinishExecution();
    }
    public class RunAndGunManager : IGameManager
    {
        private IManageable[] _manageables;

        public event Action OnRebuild;
        public event Action OnStartGame;
        public event Action OnEndGame;
        public event Action OnCompleteGame;

        public RunAndGunManager(IManageable[] manageables, Player player)
        {
            Application.targetFrameRate = 60;
            _manageables = manageables;
            player.OnDied += EndGame;
        }

        public void Rebuild()
        {
            foreach (var item in _manageables)
            {
                item.Rebuild();
            }
            OnRebuild?.Invoke();
        }

        public void StartGame()
        {
            foreach (var item in _manageables)
            {
                item.StartExecution();
            }
            OnStartGame?.Invoke();
        }

        public void EndGame()
        {
            foreach (var item in _manageables)
            {
                item.FinishExecution();
            }
            OnEndGame?.Invoke();
        }

        public void CompleteGame()
        {
            foreach (var item in _manageables)
            {
                item.FinishExecution();
            }
            OnCompleteGame?.Invoke();
        }

    }
}
