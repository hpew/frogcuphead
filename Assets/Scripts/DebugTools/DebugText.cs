using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class DebugText : MonoBehaviour, IDisposable
{
    public struct PlacedText
    {
        public string textValue;
        public DebugTextPanel panel;
        public IDisposable dispose;
    }
    [SerializeField]
    private DebugTextPanel prefab;
    [SerializeField]
    private bool isEnable = true;
    //private static Text Text;
    private static DebugTextPanel Prefab;
    private static Transform Transform;
    private static bool IsEnable = true;
    static List<PlacedText> placedTexts;
    static bool isBlack;

    private void Awake()
    {
        placedTexts = new List<PlacedText>();
        //Text = text;
        Prefab = prefab;
        Transform = transform;
        IsEnable = isEnable;
    }

    public static void Log(string value, float lifeTime = 1)
    {
        if (!IsEnable) return;
        int index = placedTexts.FindIndex(x => x.textValue == value);
        if (index >= 0) return;

        PlacedText text = new PlacedText();
        text.panel = Instantiate(Prefab, Transform);
        text.panel.text.text = value;
        text.textValue = value;
        text.panel.image.color = isBlack ? new Color(0, 0, 0, 0.5f) : new Color(0.5f, 0.5f, 0.5f, 0.5f);
        text.dispose = Observable.Timer(TimeSpan.FromSeconds(lifeTime))
            .Subscribe(_ => DestroyText(text));
        placedTexts.Add(text);
        isBlack = !isBlack;
    }

    private static void DestroyText(PlacedText text)
    {
        placedTexts.Remove(text);
        text.dispose?.Dispose();
        Destroy(text.panel.gameObject);
    }

    public void Dispose()
    {
        foreach (var item in placedTexts)
        {
            item.dispose?.Dispose();
        }
    }
}
