using UnityEngine;
using Zenject;
using Kudos.GameLogic;
using Kudos.GameControllers;
using Kudos.GameObjects;
using Kudos.SO;


public class ProjectileInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<PoolsManager>()
                .AsSingle()
                .NonLazy();

        Container.BindFactory<Projectile, Projectile.ProjectileFactory>().FromFactory<Projectile.BulletFactory>();
    }
}
