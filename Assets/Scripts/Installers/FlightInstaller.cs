using UnityEngine;
using Zenject;
using Kudos.GameLogic;
using Kudos.GameControllers;
using Kudos.GameObjects;
using Kudos.GameObjects.Bosses;
using Kudos.SO;

public class FlightInstaller : MonoInstaller
{

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<InputSystem>()
                .AsSingle()
                .NonLazy();

        Container.Bind<GameManager>()
                .AsSingle()
                .NonLazy();
    }
}