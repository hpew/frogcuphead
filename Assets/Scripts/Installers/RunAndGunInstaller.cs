using UnityEngine;
using Zenject;
using Kudos.GameLogic;
using Kudos.GameControllers;
using Kudos.GameObjects;
using Kudos.SO;

public class RunAndGunInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<InputSystem>()
                .AsSingle()
                .NonLazy();

        Container.BindInterfacesAndSelfTo<StaminaController>()
               .AsSingle()
               .NonLazy();

        Container.BindInterfacesAndSelfTo<RunAndGunManager>()
                .AsSingle()
                .NonLazy();

    }
}