using UnityEngine;
using Zenject;
using Kudos.GameLogic;
using Kudos.GameControllers;
using Kudos.GameObjects;
using Kudos.GameObjects.Bosses;
using Kudos.SO;

public class MaskBossFightInstaller : MonoInstaller
{
    [Inject] WeaponsSettingsSO _weaponsSettings;

    [SerializeField]
    private AutoAimEnemy enemypPrefab;
    [SerializeField]
    private Spear spearPrefab;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<InputSystem>()
                .AsSingle()
                .NonLazy();

        //var Weapons = _weaponsSettings.Weapons;

        //foreach (var projectile in Weapons)
        //{
        //    Container.BindMemoryPool<Projectile, Projectile.Pool>()
        //           .WithInitialSize(projectile.poolSize)
        //           .FromComponentInNewPrefab(projectile.prefab)
        //           .UnderTransformGroup("PoolContainer - " + projectile.prefab.name);
        //}

        Container.BindInterfacesAndSelfTo<RunAndGunManager>()
                .AsSingle()
                .NonLazy();

        Container.BindFactory<Enemy, AutoAimEnemy.Factory>().FromComponentInNewPrefab(enemypPrefab);
        Container.BindMemoryPool<Spear, Spear.Pool>()
            .WithInitialSize(3)
            .FromComponentInNewPrefab(spearPrefab)
            .UnderTransformGroup("PoolContainer - " + spearPrefab.name);
    }
}