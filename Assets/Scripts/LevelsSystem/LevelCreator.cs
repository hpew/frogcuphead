using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.LevelsSystem
{
    public class LevelCreator : MonoBehaviour
    {
        [SerializeField]
        private ChunkCreator startChunkPrefab;
        [SerializeField]
        private ChunkCreator finishChunkPrefab;
        [SerializeField]
        private ChunkCreator[] chunkPrefabs;

        private List<ChunkCreator> chunks;

        public void CreateLevel()
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                DestroyImmediate(transform.GetChild(i).gameObject);
            }

            if (chunks == null)
            {
                chunks = new List<ChunkCreator>();
            }
            else
            {
                chunks.Clear();
            }

            ChunkCreator tempChunk = null;
            tempChunk = Instantiate(startChunkPrefab, transform);
            chunks.Add(tempChunk);
            foreach (var prefab in chunkPrefabs)
            {
                tempChunk = Instantiate(prefab, chunks[chunks.Count - 1].EndPoint.position, Quaternion.identity, transform);
                chunks.Add(tempChunk);
            }
            tempChunk = Instantiate(finishChunkPrefab, chunks[chunks.Count - 1].EndPoint.position, Quaternion.identity, transform);
            chunks.Add(tempChunk);
        }
    }
}
