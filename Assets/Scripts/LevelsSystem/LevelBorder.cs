using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Kudos.LevelsSystem
{
    public class LevelBorder : MonoBehaviour
    {
        private void OnTriggerExit(Collider other)
        {
            IDisposable disposable = other.GetComponent<IDisposable>();
            if (disposable != null)
            {
                disposable.Dispose();
            }
        }
    }
}
