using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Kudos.LevelsSystem
{
    [CustomEditor(typeof(RandomLevelCreator)), CanEditMultipleObjects]
    public class RandomLevelCreatorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
           RandomLevelCreator levelCreator = target as RandomLevelCreator;

            if (GUILayout.Button("Generate"))
            {
                levelCreator.CreateLevel();
            }
            if (GUILayout.Button("Randomize Chunks"))
            {
                levelCreator.RandomizeChunks();
            }
            if (GUILayout.Button("Randomize Obstacles"))
            {
                levelCreator.RandomizeObstacles();
            }
        }
    }
}
