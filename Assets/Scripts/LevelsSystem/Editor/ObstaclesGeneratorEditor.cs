using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Kudos.LevelsSystem
{
    [CustomEditor(typeof(ObstaclesGenerator)), CanEditMultipleObjects]
    public class ObstaclesGeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            ObstaclesGenerator obstaclesGenerator = target as ObstaclesGenerator;
            if (GUILayout.Button("Randomize"))
            {
                obstaclesGenerator.GenerateMap();
            }
        }
    }
}
