using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Kudos.LevelsSystem
{
    [CustomEditor(typeof(LevelCreator)), CanEditMultipleObjects]
    public class LevelCreatorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            LevelCreator levelCreator = target as LevelCreator;

            if (GUILayout.Button("Generate"))
            {
                levelCreator.CreateLevel();
            }
        }
    }
}
