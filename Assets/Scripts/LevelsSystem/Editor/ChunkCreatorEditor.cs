using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.LevelsSystem;
using UnityEditor;

namespace Kudos.LevelsSystem
{
    [CustomEditor(typeof(ChunkCreator), editorForChildClasses: true), CanEditMultipleObjects]
    public class ChunkCreatorEditor : Editor
    {
        private GUIContent duplicateButtonContent = new GUIContent("+", "duplicate");
        private GUIContent deleteButtonContent = new GUIContent("-", "delete");
        private GUILayoutOption miniButtonWidth = GUILayout.Width(20f);
        public override void OnInspectorGUI()
        {
            ChunkCreator chunkCreator = target as ChunkCreator;
            EditorGUILayout.PropertyField(serializedObject.FindProperty("cubeTopPrefab"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("cubeMiddlePrefab"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("cubeBottomPrefab"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("length"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("height"));
            EditorGUILayout.Space();

            //EditorGUILayout.PropertyField(serializedObject.FindProperty("topPlatforms"), true);
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("middlePlatforms"), true);
            //EditorGUILayout.PropertyField(serializedObject.FindProperty("bottomPlatforms"), true);

            if (StartEndPoints(chunkCreator))
            {
                ShowList(serializedObject.FindProperty("topPlatforms"), chunkCreator, ChunkCreator.PlatformPositionType.top);
                ShowList(serializedObject.FindProperty("middlePlatforms"), chunkCreator, ChunkCreator.PlatformPositionType.middle);
                ShowList(serializedObject.FindProperty("bottomPlatforms"), chunkCreator, ChunkCreator.PlatformPositionType.bottom);

                if (GUILayout.Button("Generate"))
                {
                    chunkCreator.FillChunk();
                }
            }
            serializedObject.ApplyModifiedProperties();
            //base.OnInspectorGUI();
            UpdateSize(chunkCreator);
        }

        private void ShowList(SerializedProperty list, ChunkCreator chunkCreator, ChunkCreator.PlatformPositionType positionType)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(list, false);
            if (GUILayout.Button(duplicateButtonContent, EditorStyles.miniButton, miniButtonWidth))
            {

                Debug.Log("E positionType " + positionType.ToString());
                chunkCreator.AddPlatform(positionType);
                list.isExpanded = true;
            }
            EditorGUILayout.EndHorizontal();
            if (list.isExpanded)
            {
                EditorGUI.indentLevel += 1;
                for (int i = 0; i < list.arraySize; i++)
                {
                    //EditorGUILayout.BeginHorizontal();
                    //EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("startPoint"), true);
                    //EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("endPoint"), true);
                    if (GUILayout.Button(deleteButtonContent, EditorStyles.miniButton, miniButtonWidth))
                    {
                        chunkCreator.RemovePlatform(positionType, i);
                    }

                    float startLeft = chunkCreator.StartPoint.localPosition.x;

                    float startRight = chunkCreator.EndPoint.localPosition.x;

                    float startValue = chunkCreator.GetStartPos(positionType, i);
                    if (startValue < 0) startValue = startLeft;

                    float endLeft = chunkCreator.StartPoint.localPosition.x;

                    float endRight = chunkCreator.EndPoint.localPosition.x;

                    float endValue = chunkCreator.GetEndPos(positionType, i);
                    if (endValue < 0) endValue = endRight;

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Start Point");
                    float startPos = EditorGUILayout.Slider(startValue, startLeft, startRight);
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("End Point");
                    float endPos = EditorGUILayout.Slider(endValue, endLeft, endRight);
                    EditorGUILayout.EndHorizontal();
                    chunkCreator.SetUpPlatform(positionType, i, startPos, endPos);

                    //EditorGUILayout.EndHorizontal();
                }
                EditorGUI.indentLevel -= 1;
            }
        }

        private bool StartEndPoints(ChunkCreator chunkCreator)
        {
            if (chunkCreator.StartPoint == null || chunkCreator.EndPoint == null ||
                chunkCreator.TopPosition == null || chunkCreator.MiddlePosition == null || chunkCreator.BottomPosition == null)
            {
                chunkCreator.CreateStartEndPoint();
                return false;
            }
            return true;
        }

        private void UpdateSize(ChunkCreator chunkCreator)
        {
            chunkCreator.SetHeight(chunkCreator.Height);
            chunkCreator.SetLength(chunkCreator.Length);
        }
    }
}
