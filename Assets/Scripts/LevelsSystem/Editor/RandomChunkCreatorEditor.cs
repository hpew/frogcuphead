using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Kudos.LevelsSystem
{
    [CustomEditor(typeof(RandomChunkCreator)), CanEditMultipleObjects]
    public class RandomChunkCreatorEditor : ChunkCreatorEditor
    {
        public override void OnInspectorGUI()
        {
            RandomChunkCreator chunkCreator = target as RandomChunkCreator;
            base.OnInspectorGUI();
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("seed"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("useRandomSeed"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("topFillType"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("middleFillType"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("bottomFillType"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("randomFillPercent"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("targetSmooth"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("targetNeighbourTiles"));

            if (GUILayout.Button("Randomize"))
            {
                chunkCreator.GenerateMap();
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}
