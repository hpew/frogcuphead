using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.LevelsSystem
{
    [System.Serializable]
    public struct Platform
    {
        public Transform parent;
        public Transform startPoint;
        public Transform endPoint;
    }
    public class ChunkCreator : MonoBehaviour
    {
        public enum PlatformPositionType { top, middle, bottom }
        [SerializeField]
        private Transform startPoint;
        [SerializeField]
        private Transform endPoint;
        [SerializeField]
        private Transform topPosition;
        [SerializeField]
        private Transform middlePosition;
        [SerializeField]
        private Transform bottomPosition;
        [SerializeField]
        private int length;
        [SerializeField]
        private float height;
        [SerializeField]
        private float size;

        public Transform StartPoint => startPoint;
        public Transform EndPoint => endPoint;
        public Transform TopPosition => topPosition;
        public Transform MiddlePosition => middlePosition;
        public Transform BottomPosition => bottomPosition;
        public int Length => length;
        public float Height => height;
        public float Size => size;
        [SerializeField]
        private GameObject cubeTopPrefab;
        [SerializeField]
        private GameObject cubeMiddlePrefab;
        [SerializeField]
        private GameObject cubeBottomPrefab;

        [SerializeField]
        protected List<Platform> topPlatforms;
        [SerializeField]
        protected List<Platform> middlePlatforms;
        [SerializeField]
        protected List<Platform> bottomPlatforms;

        //private List<GameObject> instantiatedPlatform = new List<GameObject>();

        public void CreateStartEndPoint()
        {
            if (startPoint != null)
            {
                DestroyImmediate(startPoint.gameObject);
            }
            if (endPoint != null)
            {
                DestroyImmediate(endPoint.gameObject);
            }

            if (topPosition != null)
            {
                DestroyImmediate(topPosition.gameObject);
            }
            if (middlePosition != null)
            {
                DestroyImmediate(middlePosition.gameObject);
            }
            if (bottomPosition != null)
            {
                DestroyImmediate(bottomPosition.gameObject);
            }

            //startPoint = Instantiate(new GameObject(), transform).transform;
            startPoint = (new GameObject()).transform;
            startPoint.SetParent(transform);
            startPoint.localPosition = Vector3.zero;
            startPoint.gameObject.name = "StartPoint";

            //topPosition = Instantiate(new GameObject(), transform).transform;
            topPosition = (new GameObject()).transform;
            topPosition.SetParent(transform);
            topPosition.localPosition = Vector3.up * height;
            topPosition.gameObject.name = "Top";

            //middlePosition = Instantiate(new GameObject(), transform).transform;
            middlePosition = (new GameObject()).transform;
            middlePosition.SetParent(transform);
            middlePosition.localPosition = Vector3.zero;
            middlePosition.gameObject.name = "Middle";

            //bottomPosition = Instantiate(new GameObject(), transform).transform;
            bottomPosition = (new GameObject()).transform;
            bottomPosition.SetParent(transform);
            bottomPosition.localPosition = Vector3.down * height;
            bottomPosition.gameObject.name = "Bottom";

            //endPoint = Instantiate(new GameObject(), transform).transform;
            endPoint = (new GameObject()).transform;
            endPoint.SetParent(transform);
            endPoint.localPosition = Vector3.right * length;
            endPoint.gameObject.name = "EndPoint";

            SetHeight(Height);
        }

        public void AddPlatform(PlatformPositionType positionType)
        {
            if (positionType == PlatformPositionType.top)
            {
                Platform platform = new Platform();

                platform.parent = (new GameObject()).transform;
                platform.parent.SetParent(topPosition);
                platform.parent.localPosition = Vector3.zero;
                platform.parent.gameObject.name = "Platform." + topPlatforms.Count;

                platform.startPoint = (new GameObject()).transform;
                platform.startPoint.SetParent(platform.parent);
                platform.startPoint.localPosition = Vector3.zero;
                platform.startPoint.gameObject.name = "StartPoint";

                platform.endPoint = (new GameObject()).transform;
                platform.endPoint.SetParent(platform.parent);
                platform.endPoint.localPosition = Vector3.zero;
                platform.endPoint.gameObject.name = "EndPoint";

                topPlatforms.Add(platform);
            }
            if (positionType == PlatformPositionType.middle)
            {
                Platform platform = new Platform();

                platform.parent = (new GameObject()).transform;
                platform.parent.SetParent(middlePosition);
                platform.parent.localPosition = Vector3.zero;
                platform.parent.gameObject.name = "Platform." + middlePlatforms.Count;

                platform.startPoint = (new GameObject()).transform;
                platform.startPoint.SetParent(platform.parent);
                platform.startPoint.localPosition = Vector3.zero;
                platform.startPoint.gameObject.name = "StartPoint";

                platform.endPoint = (new GameObject()).transform;
                platform.endPoint.SetParent(platform.parent);
                platform.endPoint.localPosition = Vector3.zero;
                platform.endPoint.gameObject.name = "EndPoint";

                middlePlatforms.Add(platform);
            }
            if (positionType == PlatformPositionType.bottom)
            {
                Platform platform = new Platform();

                platform.parent = (new GameObject()).transform;
                platform.parent.SetParent(bottomPosition);
                platform.parent.localPosition = Vector3.zero;
                platform.parent.gameObject.name = "Platform." + bottomPlatforms.Count;

                platform.startPoint = (new GameObject()).transform;
                platform.startPoint.SetParent(platform.parent);
                platform.startPoint.localPosition = Vector3.zero;
                platform.startPoint.gameObject.name = "StartPoint";

                platform.endPoint = (new GameObject()).transform;
                platform.endPoint.SetParent(platform.parent);
                platform.endPoint.localPosition = Vector3.zero;
                platform.endPoint.gameObject.name = "EndPoint";

                bottomPlatforms.Add(platform);
            }

            SetHeight(Height);
        }

        public void RemovePlatform(PlatformPositionType positionType, int index)
        {
            if (positionType == PlatformPositionType.top)
            {
                if (index < topPlatforms.Count)
                {
                    DestroyImmediate(topPlatforms[index].parent.gameObject);
                    topPlatforms.RemoveAt(index);
                }
            }
            if (positionType == PlatformPositionType.middle)
            {
                if (index < middlePlatforms.Count)
                {
                    DestroyImmediate(middlePlatforms[index].parent.gameObject);
                    middlePlatforms.RemoveAt(index);
                }
            }
            if (positionType == PlatformPositionType.bottom)
            {
                if (index < bottomPlatforms.Count)
                {
                    DestroyImmediate(bottomPlatforms[index].parent.gameObject);
                    bottomPlatforms.RemoveAt(index);
                }
            }
        }

        public void FillChunk()
        {
            if (cubeTopPrefab != null && cubeMiddlePrefab != null && cubeBottomPrefab != null)
            {
                UpdateSize();

                foreach (var platform in topPlatforms)
                {
                    for (int i = platform.startPoint.childCount - 1; i >= 0; i--)
                    {
                        DestroyImmediate(platform.startPoint.GetChild(i).gameObject);
                    }
                }

                foreach (var platform in middlePlatforms)
                {
                    for (int i = platform.startPoint.childCount - 1; i >= 0; i--)
                    {
                        DestroyImmediate(platform.startPoint.GetChild(i).gameObject);
                    }
                }

                foreach (var platform in bottomPlatforms)
                {
                    for (int i = platform.startPoint.childCount - 1; i >= 0; i--)
                    {
                        DestroyImmediate(platform.startPoint.GetChild(i).gameObject);
                    }
                }

                foreach (var platform in topPlatforms)
                {
                    for (float i = platform.startPoint.position.x + size / 2; i < platform.endPoint.position.x; i += size)
                    {
                        GameObject obj = Instantiate(cubeTopPrefab, platform.startPoint);
                        obj.transform.position = new Vector3(i, platform.startPoint.position.y, platform.startPoint.position.z);
                    }
                }
                foreach (var platform in middlePlatforms)
                {
                    for (float i = platform.startPoint.position.x + size / 2; i < platform.endPoint.position.x; i += size)
                    {
                        GameObject obj = Instantiate(cubeMiddlePrefab, platform.startPoint);
                        obj.transform.position = new Vector3(i, platform.startPoint.position.y, platform.startPoint.position.z);
                    }
                }
                foreach (var platform in bottomPlatforms)
                {
                    for (float i = platform.startPoint.position.x + size / 2; i < platform.endPoint.position.x; i += size)
                    {
                        GameObject obj = Instantiate(cubeBottomPrefab, platform.startPoint);
                        obj.transform.position = new Vector3(i, platform.startPoint.position.y, platform.startPoint.position.z);
                    }
                }
            }
        }

        public void SetUpPlatform(PlatformPositionType positionType, int index, float startPos, float endPos)
        {
            if (positionType == PlatformPositionType.top)
            {
                if (index < topPlatforms.Count)
                {
                    topPlatforms[index].startPoint.localPosition = new Vector3(startPos,
                        topPlatforms[index].startPoint.localPosition.y, topPlatforms[index].startPoint.localPosition.z);
                    topPlatforms[index].endPoint.localPosition = new Vector3(endPos,
                        topPlatforms[index].endPoint.localPosition.y, topPlatforms[index].endPoint.localPosition.z);
                }
            }
            if (positionType == PlatformPositionType.middle)
            {
                if (index < middlePlatforms.Count)
                {
                    middlePlatforms[index].startPoint.localPosition = new Vector3(startPos,
                        middlePlatforms[index].startPoint.localPosition.y, middlePlatforms[index].startPoint.localPosition.z);
                    middlePlatforms[index].endPoint.localPosition = new Vector3(endPos,
                        middlePlatforms[index].endPoint.localPosition.y, middlePlatforms[index].endPoint.localPosition.z);
                }
            }
            if (positionType == PlatformPositionType.bottom)
            {
                if (index < bottomPlatforms.Count)
                {
                    bottomPlatforms[index].startPoint.localPosition = new Vector3(startPos,
                        bottomPlatforms[index].startPoint.localPosition.y, bottomPlatforms[index].startPoint.localPosition.z);
                    bottomPlatforms[index].endPoint.localPosition = new Vector3(endPos,
                        bottomPlatforms[index].endPoint.localPosition.y, bottomPlatforms[index].endPoint.localPosition.z);
                }
            }
        }

        public void SetHeight(float value)
        {
            height = value;
            topPosition.localPosition = new Vector3(topPosition.localPosition.x, height, topPosition.localPosition.z);
            bottomPosition.localPosition = new Vector3(bottomPosition.localPosition.x, -height, bottomPosition.localPosition.z);
        }

        public void SetLength(int value)
        {
            if (cubeMiddlePrefab != null)
            {
                UpdateSize();
                endPoint.localPosition = new Vector3(value * size, endPoint.localPosition.y, endPoint.localPosition.z);
            }
        }

        public void UpdateSize()
        {
            if (cubeMiddlePrefab != null)
            {
                size = cubeMiddlePrefab.GetComponent<BoxCollider>().size.x;
            }
        }

        public int GetStartPos(PlatformPositionType positionType, int index)
        {
            if (positionType == PlatformPositionType.top)
            {
                if (index >= 0 && index < topPlatforms.Count)
                {
                    return (int)topPlatforms[index].startPoint.localPosition.x;
                }
            }
            if (positionType == PlatformPositionType.middle)
            {
                if (index >= 0 && index < middlePlatforms.Count)
                {
                    return (int)middlePlatforms[index].startPoint.localPosition.x;
                }
            }
            if (positionType == PlatformPositionType.bottom)
            {
                if (index >= 0 && index < bottomPlatforms.Count)
                {
                    return (int)bottomPlatforms[index].startPoint.localPosition.x;
                }
            }
            return -1;
        }

        public int GetEndPos(PlatformPositionType positionType, int index)
        {
            if (positionType == PlatformPositionType.top)
            {
                if (index >= 0 && index < topPlatforms.Count)
                {
                    return (int)topPlatforms[index].endPoint.localPosition.x;
                }
            }
            if (positionType == PlatformPositionType.middle)
            {
                if (index >= 0 && index < middlePlatforms.Count)
                {
                    return (int)middlePlatforms[index].endPoint.localPosition.x;
                }
            }
            if (positionType == PlatformPositionType.bottom)
            {
                if (index >= 0 && index < bottomPlatforms.Count)
                {
                    return (int)bottomPlatforms[index].endPoint.localPosition.x;
                }
            }
            return -1;
        }

        public Transform[,] GetTransformMap()
        {
            Transform[,] transformMap = new Transform[length, 3];
            UpdateSize();
            for (int i = 0; i < topPlatforms.Count; i++)
            {
                for (int j = 0; j < topPlatforms[i].startPoint.childCount; j++)
                {
                    int index = (int)(topPlatforms[i].startPoint.GetChild(j).localPosition.x - size / 2);
                    transformMap[index, 0] = topPlatforms[i].startPoint.GetChild(j);
                }
            }

            for (int i = 0; i < middlePlatforms.Count; i++)
            {
                for (int j = 0; j < middlePlatforms[i].startPoint.childCount; j++)
                {
                    int index = (int)(middlePlatforms[i].startPoint.GetChild(j).localPosition.x - size / 2);
                    transformMap[index, 1] = middlePlatforms[i].startPoint.GetChild(j);
                }
            }

            for (int i = 0; i < bottomPlatforms.Count; i++)
            {
                for (int j = 0; j < bottomPlatforms[i].startPoint.childCount; j++)
                {
                    int index = (int)(bottomPlatforms[i].startPoint.GetChild(j).localPosition.x - size / 2);
                    transformMap[index, 2] = bottomPlatforms[i].startPoint.GetChild(j);
                }
            }

            return transformMap;
        }

        private void OnDrawGizmos() 
        {
            GrawGizmos();
        }

        protected void GrawGizmos()
        {
            if (startPoint != null)
            {
                Gizmos.DrawIcon(startPoint.position, "startPointGizmos");
            }
            if (endPoint != null)
            {
                Gizmos.DrawIcon(endPoint.position, "endPointGizmos");
            }

            foreach (var item in topPlatforms)
            {
                if (item.startPoint != null)
                {
                    Gizmos.DrawIcon(item.startPoint.position, "startGizmos");
                }
                if (item.endPoint != null)
                {
                    Gizmos.DrawIcon(item.endPoint.position, "endGizmos");
                }
            }

            foreach (var item in middlePlatforms)
            {
                if (item.startPoint != null)
                {
                    Gizmos.DrawIcon(item.startPoint.position, "startGizmos");
                }
                if (item.endPoint != null)
                {
                    Gizmos.DrawIcon(item.endPoint.position, "endGizmos");
                }
            }

            foreach (var item in bottomPlatforms)
            {
                if (item.startPoint != null)
                {
                    Gizmos.DrawIcon(item.startPoint.position, "startGizmos");
                }
                if (item.endPoint != null)
                {
                    Gizmos.DrawIcon(item.endPoint.position, "endGizmos");
                }
            }
        }
    }
}
