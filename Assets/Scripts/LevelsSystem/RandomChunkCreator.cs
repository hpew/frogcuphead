using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Kudos.LevelsSystem
{
    public class RandomChunkCreator : ChunkCreator
    {
        public enum FillType { empty, solid, random }
        [Header("SeedSettings")]
        public string seed;
        public bool useRandomSeed;

        [Header("Platforms")]
        public FillType topFillType;
        public FillType middleFillType;
        public FillType bottomFillType;
        [Header("Parameters")]
        [Range(0, 100)]
        public int randomFillPercent;
        [Range(0, 10)]
        public int targetSmooth;
        [Range(0, 10)]
        public int targetNeighbourTiles;

        private int[,] map;
        private string randomSeed = null;

        public void GenerateMap(string randomSeed = null)
        {
            this.randomSeed = randomSeed;

            map = new int[Length, 3];
            RandomFillMap();
            for (int i = 0; i < targetSmooth; i++)
            {
                SmoothMap();
            }

            CorrectMap();
            FillChunkWithMap();
        }

        private void RandomFillMap()
        {
            if (useRandomSeed)
            {
                if (randomSeed != null)
                    seed = randomSeed;
                else
                    seed = Time.time.ToString();
            }

            System.Random rand = new System.Random(seed.GetHashCode());

            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < Length; x++)
                {
                    map[x, y] = (rand.Next(0, 100) < randomFillPercent) ? 1 : 0;
                }
            }
        }

        private void CorrectMap()
        {
            if (topFillType != FillType.random)
            {
                for (int x = 0; x < Length; x++)
                {
                    map[x, 0] = (int)topFillType;
                }
            }
            if (middleFillType != FillType.random)
            {
                for (int x = 0; x < Length; x++)
                {
                    map[x, 1] = (int)middleFillType;
                }
            }
            if (bottomFillType != FillType.random)
            {
                for (int x = 0; x < Length; x++)
                {
                    map[x, 2] = (int)bottomFillType;
                }
            }
        }

        private void SmoothMap()
        {
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < Length; x++)
                {
                    int neighbourTiles = GetSurroundingCount(x, y);

                    if (neighbourTiles > targetNeighbourTiles)
                        map[x, y] = 1;
                    else if (neighbourTiles < targetNeighbourTiles)
                        map[x, y] = 0;
                }
            }
        }

        private int GetSurroundingCount(int gridX, int gridY)
        {
            int count = 0;
            for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
            {
                for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
                {
                    if (neighbourX >= 0 && neighbourX < Length && neighbourY >= 0 && neighbourY < 3)
                    {
                        if (neighbourX != gridX || neighbourY != gridY)
                        {
                            count += map[neighbourX, neighbourY];
                        }
                    }
                    else
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        private void FillChunkWithMap()
        {
            for (int j = topPlatforms.Count - 1; j >= 0; j--)
            {
                RemovePlatform(PlatformPositionType.top, j);
            }
            for (int j = middlePlatforms.Count - 1; j >= 0; j--)
            {
                RemovePlatform(PlatformPositionType.middle, j);
            }
            for (int j = bottomPlatforms.Count - 1; j >= 0; j--)
            {
                RemovePlatform(PlatformPositionType.bottom, j);
            }

            for (int y = 0; y < 3; y++)
            {
                int index = 0;
                float startPos = 0;
                float endPos = 0;
                bool isSpace = true;
                for (int x = 0; x < Length; x++)
                {
                    if (map[x, y] == 1)
                    {
                        if (isSpace)
                        {
                            isSpace = false;
                            //startPos = x;
                            startPos = Size*x;
                            index++;
                            AddPlatform((PlatformPositionType)y);
                        }
                        endPos+=Size;

                        SetUpPlatform((PlatformPositionType)y, index - 1, startPos, startPos + endPos);
                    }
                    else
                    {
                        isSpace = true;
                        endPos = 0;
                    }
                }
            }

            Debug.Log("Length " + Length);
            FillChunk();
        }

        private void OnDrawGizmos()
        {
            GrawGizmos();
        }

    }
}
