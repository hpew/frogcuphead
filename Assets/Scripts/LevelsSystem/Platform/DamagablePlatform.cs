using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
using Kudos.GameControllers;
using Kudos.ShadersAPI;

namespace Kudos.GameObjects
{
    public class DamagablePlatform : MonoBehaviour, IManageable, ITargetable
    {
        protected DamageEffect _damageEffect;
        IDisposable effectDispose;
        IDisposable waitDamageDispose;
        [SerializeField]
        private float startHP = 5;

        public float CurrentHP { get; private set; }

        private void Awake()
        {
            _damageEffect = GetComponentInChildren<DamageEffect>();
        }

        public void Rebuild()
        {
            gameObject.SetActive(true);
            _damageEffect.Effect(false);
            CurrentHP = startHP;
        }
        public void StartExecution()
        {

        }

        public void Activate()
        {
            
        }
        public void FinishExecution()
        {
            waitDamageDispose?.Dispose();
            effectDispose?.Dispose();
        }

        public void Hit(Projectile projectile)
        {
            //Debug.Log("hit " + projectile.Damage);
            if (projectile.Owner is Enemy) return;
            CurrentHP -= projectile.Damage;
            if (CurrentHP <= 0)
                Die();
            else
                DamageEffect();
        }

        public bool IsIgnored()
        {
            return false;
        }

        public void Die()
        {
            FinishExecution();
            gameObject.SetActive(false);
            //OnDeath?.Invoke(this);
        }

        private void DamageEffect()
        {
            if (effectDispose == null)
            {
                effectDispose = Observable.Interval(TimeSpan.FromSeconds(0.1f))
                 .Finally(() =>
                 {
                     _damageEffect.Effect(false);
                     effectDispose = null;
                 })
                 .Subscribe(_ =>
                 {
                     _damageEffect.Effect(!_damageEffect.isEffect);
                 });
            }

            waitDamageDispose?.Dispose();
            waitDamageDispose = Observable.Timer(TimeSpan.FromSeconds(0.15f))
            .Subscribe(_ =>
            {
                effectDispose?.Dispose();
            });
        }

        private void OnTriggerStay(Collider other)
        {
            Player target = other.GetComponent<Player>();
            if (target != null)
            {
                if (target.IsIgnored()) return;
                target.TakeDamage("Obstacle", 1);
            }
        }
    }
}
