using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

public class SkipablePlatform : MonoBehaviour
{
    private Collider _collider;
    private void Awake()
    {
        _collider = GetComponent<Collider>();
    }
    public void Diactivate()
    {
        _collider.enabled = false;
        Observable.Timer(TimeSpan.FromSeconds(0.1f))
            .Subscribe(_=> 
            {
                _collider.enabled = true;
            });
    }
}
