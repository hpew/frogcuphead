using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;
using Kudos.GameControllers;

namespace Kudos.GameObjects
{
    public class CollapsingPlatform : MonoBehaviour, IDisposable, IManageable
    {
        IDisposable dispose;

        private bool isCollaps;

        [SerializeField]
        private float collapseTime = 0.2f;

        public void Rebuild()
        {
            isCollaps = false;
            gameObject.SetActive(true);
        }
        public void StartExecution()
        {

        }

        public void Collaps()
        {
            if (!isCollaps)
            {
                isCollaps = true;
                gameObject.SetActive(true);
                dispose?.Dispose();
                dispose = Observable.Timer(TimeSpan.FromSeconds(collapseTime))
                    .Subscribe(_ =>
                    {
                        gameObject.SetActive(false);
                    });
            }
        }
        public void FinishExecution()
        {
            dispose?.Dispose();
        }

        public void Dispose()
        {
            FinishExecution();
        }
    }
}
