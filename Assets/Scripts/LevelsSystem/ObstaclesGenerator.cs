using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameObjects;
using Kudos.GameLogic;

namespace Kudos.LevelsSystem
{
    [System.Serializable]
    public struct Obstacle�hance
    {
        public Obstacle obstaclePrefabs;
        [Range(0, 100)]
        public int randomFillPercent;
        public float upOffset;
        public float downOffset;
    }

    public class ObstaclesGenerator : MonoBehaviour
    {
        [SerializeField]
        private ChunkCreator chunkCreator;
        [SerializeField]
        private Transform obstaclesPoint;
        public Obstacle�hance[] obstaclePrefabs;
        [Header("SeedSettings")]
        public string seed;
        public bool useRandomSeed;
        [Header("Parameters")]
        public float offset = 0.3f;
        [Range(0, 100)]
        public int randomFillPercent;
        [Range(0, 10)]
        public int targetSmooth;
        [Range(0, 10)]
        public int targetNeighbourTilesMax;
        [Range(0, 10)]
        public int targetNeighbourTilesMin;

        private Transform[,] platformMap;
        private int[,] map;

        private string randomSeed = null;

        public void GenerateMap(string randomSeed = null)
        {
            this.randomSeed = randomSeed;

            map = new int[chunkCreator.Length, 4];
            platformMap = chunkCreator.GetTransformMap();
            RandomFillMap();

            for (int i = 0; i < targetSmooth; i++)
            {
                SmoothMap();
            }

            FillObstacleWithMap();
        }

        private void RandomFillMap()
        {
            if (useRandomSeed)
            {
                if (randomSeed != null)
                    seed = randomSeed;
                else
                    seed = Time.time.ToString();
            }

            System.Random rand = new System.Random(seed.GetHashCode());

            for (int y = 0; y < 4; y++)
            {
                for (int x = 0; x < chunkCreator.Length; x++)
                {
                    int py = y >= 2 ? y - 1 : y;
                    if (platformMap[x, py] != null)
                    {
                        map[x, y] = (rand.Next(0, 100) < randomFillPercent) ? 1 : 0;
                    }
                    else
                    {
                        map[x, y] = 0;
                    }
                }
            }
        }

        private void SmoothMap()
        {
            for (int y = 0; y < 4; y++)
            {
                for (int x = 0; x < chunkCreator.Length; x++)
                {
                    int neighbourTiles = GetSurroundingCount(x, y);

                    //Debug.Log("neighbourTiles " + neighbourTiles);
                    if (neighbourTiles > targetNeighbourTilesMax)
                        map[x, y] = 1;
                    else if (neighbourTiles < targetNeighbourTilesMin)
                        map[x, y] = 0;
                }
            }
        }

        private int GetSurroundingCount(int gridX, int gridY)
        {
            //����� �������� ��� ���������
            int count = 0;
            for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
            {
                for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
                {
                    if (neighbourX >= 0 && neighbourX < chunkCreator.Length && neighbourY >= 0 && neighbourY < 4)
                    {
                        if (neighbourX != gridX || neighbourY != gridY)
                        {
                            count += map[neighbourX, neighbourY];
                        }
                    }
                    else
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        private int GetSurroundingUpCount(int gridX, int gridY)
        {
            int count = 0;
            for (int y = 0; y < 4; y++)
            {

            }
            return count;
        }

        private void FillObstacleWithMap()
        {
            WeightedRandom<Obstacle�hance> random = new WeightedRandom<Obstacle�hance>(seed);
            foreach (var prefab in obstaclePrefabs)
            {
                random.AddEntry(prefab, prefab.randomFillPercent);
            }

            for (int i = obstaclesPoint.childCount - 1; i >= 0; i--)
            {
                DestroyImmediate(obstaclesPoint.GetChild(i).gameObject);
            }

            for (int y = 0; y < 4; y++)
            {
                for (int x = 0; x < chunkCreator.Length; x++)
                {
                    Obstacle�hance obstacle = random.GetRandom();
                    if (map[x, y] == 0) continue;
                        Vector3 pos = Vector3.zero;
                    switch (y)
                    {
                        case 0:
                            if (platformMap[x, y] == null) continue;
                            pos = platformMap[x, y].position - Vector3.up * obstacle.downOffset;
                            break;
                        case 1:
                            if (platformMap[x, y] == null) continue;
                            pos = platformMap[x, y].position + Vector3.up * obstacle.upOffset;
                            break;
                        case 2:
                            if (platformMap[x, y - 1] == null) continue;
                            pos = platformMap[x, y - 1].position - Vector3.up * obstacle.downOffset;
                            break;
                        case 3:
                            if (platformMap[x, y - 1] == null) continue;
                            pos = platformMap[x, y - 1].position + Vector3.up * obstacle.upOffset;
                            break;
                        default:
                            continue;
                    }

                    Instantiate(obstacle.obstaclePrefabs, pos, obstacle.obstaclePrefabs.transform.rotation, obstaclesPoint);
                }
            }
        }

    }
}
