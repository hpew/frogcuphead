using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.LevelsSystem
{
    public class RandomLevelCreator : MonoBehaviour
    {
        [SerializeField]
        private ChunkCreator startChunkPrefab;
        [SerializeField]
        private ChunkCreator finishChunkPrefab;
        [SerializeField]
        private RandomChunkCreator chunkPrefabs;
        [SerializeField]
        private int chunkCount;
        [Header("SeedSettings")]
        public string seed;
        public bool useRandomSeed;

        [SerializeField]
        private List<ChunkCreator> chunks;

        public void CreateLevel()
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                DestroyImmediate(transform.GetChild(i).gameObject);
            }

            if (chunks == null)
            {
                chunks = new List<ChunkCreator>();
            }
            else
            {
                chunks.Clear();
            }

            ChunkCreator tempChunk = null;
            tempChunk = Instantiate(startChunkPrefab, transform);
            chunks.Add(tempChunk);
            for (int i = 0; i < chunkCount; i++)
            {
                tempChunk = Instantiate(chunkPrefabs, chunks[chunks.Count - 1].EndPoint.position, Quaternion.identity, transform);
                chunks.Add(tempChunk);
            }
            tempChunk = Instantiate(finishChunkPrefab, chunks[chunks.Count - 1].EndPoint.position, Quaternion.identity, transform);
            chunks.Add(tempChunk);
        }

        public void RandomizeChunks()
        {
            if (useRandomSeed)
            {
                seed = Time.time.ToString();
            }

            System.Random rand = new System.Random(seed.GetHashCode());
            
            for (int i = 1; i < chunks.Count-1; i++)
            {
                (chunks[i] as RandomChunkCreator).GenerateMap(rand.NextDouble().ToString());
            }
        }
        public void RandomizeObstacles()
        {
            if (useRandomSeed)
            {
                seed = Time.time.ToString();
            }

            System.Random rand = new System.Random(seed.GetHashCode());

            for (int i = 1; i < chunks.Count - 1; i++)
            {
                (chunks[i] as RandomChunkCreator).GetComponent<ObstaclesGenerator>().GenerateMap(rand.NextDouble().ToString());
            }
        }
    }
}
