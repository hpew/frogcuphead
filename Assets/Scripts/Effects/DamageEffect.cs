using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.ShadersAPI
{
    public class DamageEffect : MonoBehaviour
    {
        //[SerializeField]
        private Renderer[] _renderers;
        [SerializeField]
        private Color startColor;
        public bool isEffect { get; private set; }
        public Color StartColor => startColor;

        private void Awake()
        {
            _renderers = GetComponentsInChildren<Renderer>();
        }
        public void Effect(bool value)
        {
            isEffect = value;
            
            if (value)
            {
                foreach (var _material in _renderers)
                {
                    _material.material.color = Color.white;
                }
            }
            else
            {
                foreach (var _material in _renderers)
                {
                    _material.material.color = startColor;
                }
            }
        }
    }
}
