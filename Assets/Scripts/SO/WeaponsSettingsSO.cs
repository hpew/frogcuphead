using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameObjects;

namespace Kudos.SO
{
    [CreateAssetMenu(fileName = "WeaponsSettings", menuName = "SO/WeaponsSettings")]
    public class WeaponsSettingsSO : ScriptableObject
    {
    }
}
