using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameLogic;

namespace Kudos.GameObjects
{
    public class ActivateButton : TakeableObject
    {
        public GameObject[] obstacles;
        public bool isActivate = true;
        private List<ILockable> activatables;

        private void Start()
        {
            activatables = new List<ILockable>();
            foreach (var item in obstacles)
            {
                activatables.Add(item.GetComponent<ILockable>());
            }
        }
        public override void Init()
        {
            _collider.enabled = true;
            foreach (var activatable in activatables)
            {
                if (activatable != null)
                    activatable.Init(!isActivate);
            }
        }

        public override void PickUp(Object picker, CollisionInfo collision)
        {
            _collider.enabled = false;
            Debug.Log("PickUp");
            foreach (var activatable in activatables)
            {
                if (activatable != null)
                    activatable.Lock(isActivate);
            }
        }
    }
}
