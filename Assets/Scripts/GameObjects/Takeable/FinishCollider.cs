using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameLogic;
using Kudos.GameControllers;
using UniRx;
using Zenject;

namespace Kudos.GameObjects
{
    public class FinishCollider : TakeableObject
    {
        [Inject] IGameManager manager;
        public override void Init()
        {
            
        }

        public override void PickUp(Object picker, CollisionInfo collision)
        {
            manager.CompleteGame();
            (picker as RunPlayer)?.Dispose();
        }
    }
}
