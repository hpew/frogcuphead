using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameLogic;

namespace Kudos.GameObjects
{
    public class DashBooster : TakeableObject
    {
        public Transform target;
        public float time = 0.5f;
        public override void Init()
        {
            gameObject.SetActive(true);
        }

        public override void PickUp(Object picker, CollisionInfo collision)
        {
            gameObject.SetActive(false);
            Dash(picker as RunPlayer);
        }

        private void Dash(RunPlayer player)
        {
            if (player != null && target != null)
            {
                player.MoveToward(transform.position, target.position, time);
            }
        }
    }
}
