using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameLogic;
using Kudos.GameControllers;

namespace Kudos.GameObjects
{
    public abstract class TakeableObject : MonoBehaviour, IIgnorable, IManageable
    {
        protected Collider _collider;
        public abstract void Init();
        public abstract void PickUp(Object picker, CollisionInfo collision);

        public void Rebuild()
        {
            Init();
        }
        public void StartExecution()
        {

        }
        public void FinishExecution()
        {

        }

        private void Awake()
        {
            _collider = GetComponent<Collider>();
        }
    }
}
