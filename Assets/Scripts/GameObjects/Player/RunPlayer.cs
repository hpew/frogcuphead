using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Zenject;
using System;
using Kudos.GameLogic;
using Kudos.ShadersAPI;

namespace Kudos.GameObjects
{
    public class RunPlayer : Player
    {
        [Inject] StaminaController _stamina;
        [Inject] GameControl _gameControl;

        [SerializeField]
        private float startStamina = 100;
        [SerializeField]
        private float moveSpeed = 6;
        [SerializeField]
        private float acceleration = 0.5f;
        [SerializeField]
        private float gravity = -20;
        [SerializeField]
        private float timeToJumpApex = 0.4f;
        [SerializeField]
        private Vector3 velocity;
        [SerializeField]
        private float upClamp;
        [SerializeField]
        private float downClamp;

        private float jumpVelocity;
        private float velocityScale = 1;
        private Vector3 lastGroundPos;
        private float lastGroundGravity;

        private IDisposable moveDispose;
        private IDisposable inputDispose;
        private IDisposable moveTowardDispose;
        private IDisposable dashDispose;
        private IDisposable lerpVelocityScaleDispose;

        public float MoveSpeed => moveSpeed;
        public float Acceleration => acceleration;
        public float Gravity => gravity;
        public Vector3 Velocity => velocity;

        private bool isMove = false;
        private bool canDash = true;

        Vector3[] downRot = { new Vector3(0, 0, 0), new Vector3(180, 180, 180), new Vector3(180, 180, -180),
                              new Vector3(180, -180, 180), new Vector3(-180, 180, 180), new Vector3(180, -180, -180),
                              new Vector3(-180, 180, -180), new Vector3(-180, -180, 180), new Vector3(-180, -180, -180)};
        Vector3[] upRot = { new Vector3(180, 0, 0), new Vector3(-180, 0, 0), new Vector3(0, 180, 180),
                            new Vector3(0, 180, -180), new Vector3(0, -180, 180), new Vector3(0, -180, -180)};

        private List<CollapsingPlatform> collapsingPlatforms;

        public override void InitializeSelf()
        {
            collapsingPlatforms = new List<CollapsingPlatform>();
        }

        protected override void RebuildSelf()
        {
            gravity = -Mathf.Abs(gravity);
            _animator.transform.parent.eulerAngles = Vector3.zero;
            velocityScale = 1;
            _cc.IsIgnore = false;
            _blinkingEffect.Blink(false);
            _stamina.ReInit(startStamina);
            canDash = true;
            collapsingPlatforms.Clear();
        }

        protected override void StartExecutionSelf()
        {
            InputSystem.OnInputUpdate += ReceiveInput;
            _gameControl.dashButton.onClick.AddListener(Dash);
            moveDispose = Observable.EveryFixedUpdate()
                .Subscribe(_ => Move());
        }

        protected override void FinishExecutionSelf()
        {
            _stamina.Dispose();
            gun.Dispose();
            inputDispose?.Dispose();
            moveDispose?.Dispose();
            moveTowardDispose?.Dispose();
            dashDispose?.Dispose();
            lerpVelocityScaleDispose?.Dispose();
            InputSystem.OnInputUpdate -= ReceiveInput;
            //InputSystem.OnMouseButtonDown -= ChangeGravity;
            _gameControl.dashButton.onClick.RemoveListener(Dash);
        }

        private void ReceiveInput(InputInfo[] inputs)
        {
            bool isHold = false;
            int k = 0;
            foreach (var input in inputs)
            {
                k++;
                DebugText.Log("input " + k + ") " + input.inputType.ToString());
                if (input.touchPosition.x > Screen.width / 2)
                {
                    if (input.inputType == InputType.upSwipe)
                    {
                        if (gravity < 0)
                            ChangeGravity();
                        DebugText.Log("InputType.upSwipe");
                    }
                    else if (input.inputType == InputType.downSwipe)
                    {
                        if (gravity > 0)
                            ChangeGravity();
                        DebugText.Log("InputType.downSwipe");
                    }

                    if (input.inputType == InputType.rightSwipe && canDash && _stamina.SubstractStamina(startStamina / 2))
                    {
                        Dash(2, 0.1f);
                        DebugText.Log("dash");
                    }
                }

                if (input.IsHold() && input.touchPosition.x < Screen.width / 2 || Input.GetKey(KeyCode.Space))
                {
                    DebugText.Log("hold");
                    isHold = true;                   
                }
            }
            if (isHold)
            {
                _stamina.IsRegaining = true;
                isMove = true;
                DebugText.Log("InputType.hold");
            }
            else
            {
                isMove = false;
                velocity.x = 0;
                _stamina.IsRegaining = false;
            }
        }

        private IDisposable LerpVelocityScale(float scale, float time)
        {
            float ElapsedTime = 0;
            float StartValue = velocityScale;
            IDisposable dispose = null;
            dispose = Observable.EveryUpdate()
                .Subscribe(_ =>
                {
                    ElapsedTime += Time.fixedDeltaTime;
                    //everyTime?.Invoke(ElapsedTime);
                    velocityScale = Mathf.Lerp(StartValue, scale, ElapsedTime / time);
                    if (ElapsedTime >= time)
                    {
                        dispose?.Dispose();
                        //complete?.Invoke();
                    }
                });
            return dispose;
        }

        public override void Hit(Projectile projectile)
        {
            base.Hit(projectile);
        }

        public override void TakeDamage(string name, int damage = 1)
        {
            base.TakeDamage(name, damage);
        }

        protected override void Die(string death)
        {
            base.Die(death);
        }

        private void Move()
        {
            if (_cc.Collisions.collisionSide == CollisionSideType.above || _cc.Collisions.collisionSide == CollisionSideType.below)
            {
                //velocity.y = 0;
                velocity.y = -jumpVelocity;
                jumpVelocity = 0;
                _animator.SetTrigger("Run");
                if (Time.time % 1 == 0)
                {
                    lastGroundPos = transform.position;
                    if (_cc.Collisions.collisionSide == CollisionSideType.above)
                    {
                        lastGroundGravity = Mathf.Abs(gravity);
                    }
                    else if (_cc.Collisions.collisionSide == CollisionSideType.below)
                    {
                        lastGroundGravity = -Mathf.Abs(gravity);
                    }
                    //Debug.Log("lastGroundPos");
                }
            }

            if (isMove)
            {
                velocity.x = Mathf.MoveTowards(velocity.x, moveSpeed * Time.fixedDeltaTime, acceleration * Time.fixedDeltaTime);
            }
            //velocity.x = moveSpeed * Time.fixedDeltaTime;
            velocity.y += gravity * Time.fixedDeltaTime;
            transform.Translate(_cc.Move(velocityScale * velocity));
            if (transform.position.y >= upClamp || transform.position.y <= downClamp)
            {
                TakeDamage("void", 1);
                transform.position = lastGroundPos;
                if (gravity != lastGroundGravity)
                {
                    ChangeGravity();
                }
                foreach (var item in collapsingPlatforms)
                {
                    item.Rebuild();
                }
                collapsingPlatforms.Clear();
            }
        }

        public void MoveToward(Vector3 current, Vector3 target, float speed)
        {
            //if (IsInvincible) return;
            Debug.Log("start MoveToward");
            moveTowardDispose?.Dispose();
            moveDispose?.Dispose();
            velocity.y = 0;
            //IsInvincible = true;
            float currentSpeed = speed;
            moveTowardDispose = Observable.EveryFixedUpdate()
                .Subscribe(_ =>
                {
                    if (target.magnitude - transform.position.magnitude > 0.01f)
                    {
                        currentSpeed = Mathf.MoveTowards(currentSpeed, moveSpeed, speed * Time.fixedDeltaTime);
                        Vector3 path = new Vector3(Vector3.MoveTowards(transform.position, target, currentSpeed * Time.fixedDeltaTime).x,
                            target.y, transform.position.z);
                        velocity = path - transform.position;
                        transform.Translate(_cc.Move(velocity));
                        //transform.position = path;

                        Debug.Log("velocity " + velocity.x);
                    }
                    else
                    {
                        moveTowardDispose?.Dispose();
                        //IsInvincible = false;
                        moveDispose = Observable.EveryFixedUpdate()
                                                .Subscribe(_ => Move());
                        Debug.Log("end MoveToward");
                    }
                });
        }

        public void Dash()
        {
            RaycastHit[] hits = Physics.RaycastAll(transform.position, 2 * Vector3.right, 2);
            bool isHit = false;
            foreach (var hit in hits)
            {
                //Debug.Log("hit " + hit.collider.name);
                if (hit.collider.GetComponent<Obstacle>() == null && hit.collider.GetComponent<Projectile>() == null && hit.collider.GetComponent<TakeableObject>() == null)
                {
                    isHit = true;
                }
            }
            if (canDash && !isHit && _stamina.SubstractStamina(startStamina / 2))
            {
                Dash(2, 0.1f);
                DebugText.Log("Dash");
            }
        }

        public void Dash(float distance, float speed)
        {
            canDash = false;
            dashDispose?.Dispose();
            moveDispose?.Dispose();
            float startY = velocity.y;
            velocity.y = 0;
            IsInvincible = true;

            dashDispose = LerpMove(transform, transform.position + Vector3.right * distance, speed, (x) =>
            {

            }, () =>
            {
                IsInvincible = false;
                velocity.y = startY;
                canDash = true;
                moveDispose = Observable.EveryFixedUpdate()
                                        .Subscribe(_ => Move());
            });
        }

        private IDisposable LerpMove(Transform target, Vector3 to, float time, Action<float> everyTime, Action complete)
        {
            float ElapsedTime = 0;
            Vector3 StartPosition = target.position;
            IDisposable dispose = null;
            dispose = Observable.EveryFixedUpdate()
                .Subscribe(_ =>
                {
                    ElapsedTime += Time.fixedDeltaTime;
                    everyTime?.Invoke(ElapsedTime);
                    target.position = Vector3.Lerp(StartPosition, to, ElapsedTime / time);
                    if (ElapsedTime >= time)
                    {
                        dispose?.Dispose();
                        complete?.Invoke();
                    }
                });
            return dispose;
        }

        public void Jump(float timeToJumpApex)
        {
            jumpVelocity = gravity * timeToJumpApex;
        }

        private void ChangeGravity()
        {
            //if (_cc.Collisions.collisionSide != CollisionSideType.none)
            {
                //velocity.y = -jumpVelocity;
                jumpVelocity = gravity * timeToJumpApex;
                gravity = -gravity;

                _animator.SetTrigger("Jump");
                if (gravity > 0)
                    LeanTween.rotate(_animator.transform.parent.gameObject, upRot[UnityEngine.Random.Range(0, upRot.Length)], 0.1f);
                else if (gravity < 0)
                    LeanTween.rotate(_animator.transform.parent.gameObject, downRot[UnityEngine.Random.Range(0, downRot.Length)], 0.1f);
            }
        }

        protected override void DetectCollision(CollisionInfo collision)
        {
            Obstacle obstacle = collision.collider.GetComponent<Obstacle>();
            if (obstacle != null)
            {
                TakeDamage(obstacle.name);
            }
            Take(collision.collider.GetComponent<TakeableObject>(), collision);

            CollapsingPlatform platform = collision.collider.GetComponent<CollapsingPlatform>();

            if (platform != null)
            {
                platform.Collaps();
                collapsingPlatforms.Add(platform);
            }
        }

        private void Take(TakeableObject takeable, CollisionInfo collision)
        {
            if (takeable == null) return;
            takeable.PickUp(this, collision);
        }
    }
}
