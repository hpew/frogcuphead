using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Zenject;
using System;
using Kudos.GameLogic;
using Kudos.SO;

namespace Kudos.GameObjects
{
    public class PlanePlayer : Player
    {
        private DragTranslateMovement _mc;
        [Inject] FlightMovementSettings flightSettings;

        private Vector3 oldPosition;
        private Vector3 axisDirection;

        [Header("Plane Input Settings"), SerializeField]
        private float rollSpeed = 10;
        [SerializeField]
        private ClampPlace clamp;

        private IDisposable moveDispose;
        public override void InitializeSelf()
        {
            _mc = new DragTranslateMovement(flightSettings, clamp);
        }

        protected override void RebuildSelf()
        {
            
        }

        protected override void StartExecutionSelf()
        {
            moveDispose = Observable.EveryFixedUpdate()
                .Subscribe(_ => Move());
        }

        protected override void FinishExecutionSelf()
        {
            gun.Dispose();
            moveDispose?.Dispose();
        }

        private void Move()
        {
            oldPosition = transform.position;
            _mc.Move(transform);
            _cc.Move(transform.position - oldPosition);
            var targetDir = (transform.position - oldPosition) * rollSpeed;
            //axisDirection = Vector3.MoveTowards(axisDirection, targetDir, Time.deltaTime * rollSpeed);
            axisDirection = targetDir;
            _animator.SetFloat("AxisHorizontal", axisDirection.x);
            _animator.SetFloat("AxisVertical", axisDirection.y);
        }

        protected override void DetectCollision(CollisionInfo collision)
        {
            Obstacle obstacle = collision.collider.GetComponent<Obstacle>();
            if (obstacle != null)
            {
                TakeDamage(obstacle.name);
                return;
            }
        }
    }
}
