using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameLogic;
using Zenject;
using Kudos.GameControllers;

namespace Kudos.GameObjects
{
    public abstract class Projectile : MonoBehaviour
    {
        private float currentLifeTime;
        protected BoxCollider _collider;
        protected Kudos.ShadersAPI.Bullet.APIBullet _effect;
        //protected ProjectileMovementController _mc;
        [SerializeField]
        private float moveSpeed = 6;
        [SerializeField]
        private float TargetLifeTime = 10;
        [SerializeField]
        private float damage = 10;
        [SerializeField]
        private float trailTime = 0.1f;
        [SerializeField]
        protected Vector3 velocity;

        public Vector3 Velocity => velocity;
        public float MoveSpeed => moveSpeed;
        public float Damage => damage;

        public UnityEngine.Object Owner { get; private set; }

        public Action<Projectile> DestroyHandler;

        private void Init()
        {
            _collider = GetComponent<BoxCollider>();
            _effect = GetComponentInChildren<Kudos.ShadersAPI.Bullet.APIBullet>();
            //_mc = new ProjectileMovementController(_collider, 1, 1);
            //_mc.OnCollision += DetectCollision;
        }

        protected abstract void Shooting();
        protected abstract void Movement();
        protected abstract void DestroyedSelf();
        protected virtual void DetectCollision(CollisionInfo collision)
        {
            ITargetable target = collision.collider.GetComponent<ITargetable>();
            if (target is RunPlayer && Owner is RunPlayer || target is Enemy && Owner is Enemy) return;

            if (target != null)
            {
                target.Hit(this);
            }
            MustDestroy();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Projectile>() != null) return;
            if (other.GetComponent<IIgnorable>() != null) return;

            ITargetable target = other.GetComponent<ITargetable>();
            //if (this is MaskBullet)
            //{
            //    Debug.Log("target is Enemy -" + (target is Enemy) + "- Owner is Enemy -" + (Owner is Enemy) + " other.name " + other.name);
            //}
            if (target is Player && Owner is Player || target is Enemy && Owner is Enemy) return;

            if (target != null)
            {
                if (target.IsIgnored()) return;
                target.Hit(this);
            }
            MustDestroy();
        }

        protected void Move()
        {
            Movement();
            currentLifeTime += Time.deltaTime;
            if (currentLifeTime >= TargetLifeTime)
            {
                MustDestroy();
            }
        }

        private void Shoot(UnityEngine.Object owner, Action<Projectile> callback)
        {
            Owner = owner;
            DestroyHandler = callback;
            currentLifeTime = 0;
            //_mc.OnCollision += DetectCollision;
            Shooting();
        }

        private void MustDestroy()
        {
            DestroyHandler?.Invoke(this);
            DestroyHandler = null;
        }

        private void Destroyed()
        {
            //_mc.OnCollision -= DetectCollision;
            DestroyedSelf();
        }

        public class Pool : MonoMemoryPool<UnityEngine.Object, Vector3, Quaternion, Action<Projectile>, Projectile>, IValidatable
        {
            public Projectile projectileType { get; private set; }
            private Vector3 scale;
            protected override void OnCreated(Projectile projectile)
            {
                base.OnCreated(projectile);
                projectile.name = "Point";
                projectile.Init();
                projectileType = projectile;
                scale = projectile.transform.localScale;
            }
            protected override void Reinitialize(UnityEngine.Object owner, Vector3 position, Quaternion rotation, Action<Projectile> callback, Projectile projectile)
            {
                projectile.transform.position = position;
                projectile.transform.rotation = rotation;
                projectile.transform.localScale = scale;
                projectile.Shoot(owner, callback);
                projectile._effect.ChangeTimeTrailBullet(projectile.trailTime, position);
            }

            protected override void OnDespawned(Projectile projectile)
            {
                if (projectile != null)
                {
                    //projectile._effect.ChangeTimeTrailBullet(0);
                    projectile.Destroyed();
                    base.OnDespawned(projectile);
                }
            }
        }

        public class ProjectileFactory : PlaceholderFactory<Projectile>
        {
        }

        public class BulletFactory : IFactory<Projectile>
        {
            DiContainer _container;
            PoolsManager _manager;
            public BulletFactory(DiContainer container, PoolsManager manager)
            {
                _container = container;
                _manager = manager;
            }
            public Projectile Create()
            {
                Projectile projectile = _container.InstantiatePrefabForComponent<Projectile>(_manager.TargetPrefab);
                projectile.transform.SetParent(_manager.TargetTransformGroup);
                return projectile;
            }
        }
    }
}

