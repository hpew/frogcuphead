using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace Kudos.GameObjects
{
    public class SphereGun : Gun
    {
        private IDisposable timerDispose;
        [SerializeField, Header("Params")]
        float radius = 10;
        [SerializeField]
        int deltaAngle = 10;
        protected override void InitSelf()
        {
        }

        public override void StartShoot(UnityEngine.Object owner)
        {
            this.owner = owner;
            //timerDispose = Observable.Timer(TimeSpan.FromSeconds(deltaShootTime))
            //    .Repeat()
            //    .Subscribe(_ => Shot());
            Shot();
        }

        public override void StopShoot()
        {
            timerDispose?.Dispose();
        }

        protected override void Shot()
        {
            Action<Projectile> handler = delegate (Projectile projectile)
            {
                activeProjectiles.Remove(projectile);
                if (projectile.gameObject.activeSelf)
                    targetPool.Despawn(projectile);
            };
            int angle = deltaAngle + UnityEngine.Random.Range(0, 2) * deltaAngle / 2;
            for (int i = 0; i < 360; i += angle)
            {
                Vector3 dir = new Vector3(radius * Mathf.Cos(i), radius * Mathf.Sin(i), 0);
                activeProjectiles.Add(targetPool.Spawn(owner, gunpoint.position + dir,
                     Quaternion.Euler(0, 180, i), handler));
            }
        }

        protected override void DisposeSelf()
        {
            timerDispose?.Dispose();
        }
    }
}