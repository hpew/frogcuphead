using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace Kudos.GameObjects
{
    public class PlaneGun : Gun
    {
        private int bulletSpread;
        private IDisposable timerDispose;
        protected override void InitSelf()
        {
        }

        public override void StartShoot(UnityEngine.Object owner)
        {
            this.owner = owner;
            timerDispose = Observable.Timer(TimeSpan.FromSeconds(deltaShootTime))
                .Repeat()
                .Subscribe(_ => Shot());
        }

        public override void StopShoot()
        {
            timerDispose?.Dispose();
        }

        protected override void Shot()
        {
            Action<Projectile> handler = delegate (Projectile projectile)
            {
                activeProjectiles.Remove(projectile);
                if (projectile.gameObject.activeSelf)
                    targetPool.Despawn(projectile);
            };
            bulletSpread = UnityEngine.Random.Range(-1, 2);
            Projectile projectile = targetPool.Spawn(owner, gunpoint.position + Vector3.up * .1f * bulletSpread, gunpoint.rotation, handler);

            //projectile.transform.SetParent(gunpoint);
            activeProjectiles.Add(projectile);
        }

        protected override void DisposeSelf()
        {
            timerDispose?.Dispose();
        }
    }
}
