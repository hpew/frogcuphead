using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace Kudos.GameObjects
{
    public class MaskSpreadGun : Gun
    {
        private IDisposable timerDispose;
        [SerializeField, Header("Params")]
        float verticalSpeed = 10;
        [SerializeField]
        float spread = 10;

        protected override void InitSelf()
        {
        }

        public override void StartShoot(UnityEngine.Object owner)
        {
            this.owner = owner;
            timerDispose = Observable.Timer(TimeSpan.FromSeconds(deltaShootTime))
                .Repeat()
                .Subscribe(_ => Shot());
        }

        public override void StopShoot()
        {
            timerDispose?.Dispose();
        }

        protected override void Shot()
        {
            Action<Projectile> handler = delegate (Projectile projectile)
            {
                activeProjectiles.Remove(projectile);
                if (projectile.gameObject.activeSelf)
                    targetPool.Despawn(projectile);
            };
            activeProjectiles.Add(targetPool.Spawn(owner, new Vector3(gunpoint.position.x, gunpoint.position.y, 0),
                Quaternion.Euler(0, 180, Mathf.Sin(Time.time * verticalSpeed) * spread), handler));
        }

        protected override void DisposeSelf()
        {
            timerDispose?.Dispose();
        }
    }
}