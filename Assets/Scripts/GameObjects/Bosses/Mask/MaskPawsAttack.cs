using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using Zenject;
using Kudos.GameLogic;

namespace Kudos.GameObjects.Bosses
{
    public class MaskPawsAttack : MonoBehaviour, IDisposable
    {
        public Transform[] externalPaws;
        public Transform[] internalPaws;
        public Transform pivot;

        public float height = 1;
        public float rightOffset = 1;
        public float speed = 10;
        public float externalInterval = 1;
        public float internalInterval = 1;
        public float externalStartFocusOffset = 1;
        public float internalStartFocusOffset = 1;
        public float externalDeltaOffset = 1;
        public float internalDeltaOffset = 1;
        public float delay = 1;

        private Vector3[] externalStartPos;
        private Vector3[] internalStartPos;

        private float[] externalTimes;
        private float[] externalFocus;
        private int[] externalLap;
        private float[] internalTimes;
        private float[] internalFocus;
        private int[] internalLap;
        private int completeCount;

        IDisposable[] externalDispose;
        IDisposable[] internalDispose;
        IDisposable[] externalDelayDispose;
        IDisposable[] internalDelayDispose;
        List<IDisposable> moveDisposes;

        private void Start()
        {
            moveDisposes = new List<IDisposable>();
            externalTimes = new float[externalPaws.Length];
            externalFocus = new float[externalPaws.Length];
            externalLap = new int[externalPaws.Length];
            externalStartPos = new Vector3[externalPaws.Length];
            externalDispose = new IDisposable[externalPaws.Length];
            externalDelayDispose = new IDisposable[externalPaws.Length];
            internalTimes = new float[internalPaws.Length];
            internalFocus = new float[internalPaws.Length];
            internalLap = new int[internalPaws.Length];
            internalStartPos = new Vector3[internalPaws.Length];
            internalDispose = new IDisposable[internalPaws.Length];
            internalDelayDispose = new IDisposable[internalPaws.Length];
        }

        public void ReInit(Action callback)
        {
            completeCount = 0;
            for (int i = 0; i < internalPaws.Length; i++)
            {
                internalStartPos[i] = internalPaws[i].position;
            }

            for (int i = 0; i < externalPaws.Length; i++)
            {
                externalStartPos[i] = externalPaws[i].position;
            }
            for (int i = 0; i < internalTimes.Length; i++)
            {
                internalTimes[i] = 0.25f * internalInterval * Mathf.Deg2Rad - internalInterval * Mathf.Deg2Rad * i;
                internalLap[i] = 0;
                internalFocus[i] = pivot.position.x + internalStartFocusOffset;
            }
            for (int i = 0; i < externalTimes.Length; i++)
            {
                externalTimes[i] = 2 * externalInterval * Mathf.Deg2Rad - externalInterval * Mathf.Deg2Rad * i;
                externalLap[i] = 0;
                externalFocus[i] = pivot.position.x + externalStartFocusOffset;
            }
            foreach (var item in internalDispose)
            {
                item?.Dispose();
            }
            foreach (var item in externalDispose)
            {
                item?.Dispose();
            }
            for (int i = 0; i < internalPaws.Length; i++)
            {
                int j = i;
                Move(internalPaws[j], CalculateEllipse(internalTimes[j], internalFocus[j]), delay, () =>
                {
                    internalDispose[j] = Observable.EveryLateUpdate()
                        .Subscribe(_ =>
                        {
                            internalPaws[j].position = CalculateEllipse(internalTimes[j], internalFocus[j]);

                            internalTimes[j] += speed * Time.deltaTime;

                            if (internalLap[j] < 2)
                            {
                                if (internalTimes[j] >= 2 * Mathf.PI)
                                {
                                    internalTimes[j] = 0;
                                    internalLap[j]++;
                                    internalFocus[j] += internalDeltaOffset;
                                }
                            }
                            else if (internalLap[j] < 3)
                            {
                                if (internalTimes[j] >= 2 * Mathf.PI)
                                {
                                    internalTimes[j] = 0;
                                    internalLap[j]++;
                                    internalFocus[j] -= internalDeltaOffset;
                                }
                            }
                            else
                            {
                                if (internalTimes[j] >= 1.75f * Mathf.PI + 0.25f * internalInterval * Mathf.Deg2Rad - internalInterval * Mathf.Deg2Rad * j)
                                {
                                    internalDispose[j]?.Dispose();
                                    //internalDelayDispose[j]?.Dispose();
                                    //internalDelayDispose[j] = Observable.Timer(TimeSpan.FromSeconds(0.5f))
                                    Move(internalPaws[j], internalPaws[j].position, 0.5f, () =>
                                        Move(internalPaws[j], internalStartPos[j], delay, () =>
                                        {
                                            completeCount++;
                                            if (completeCount >= internalPaws.Length + externalPaws.Length)
                                            {
                                                callback?.Invoke();
                                            }
                                        }));
                                }
                            }
                        });
                });
            }
            for (int i = 0; i < externalPaws.Length; i++)
            {
                int j = i;

                Move(externalPaws[j], CalculateEllipse(externalTimes[j], externalFocus[j]), delay, () =>
                    {
                        externalDispose[j] = Observable.EveryLateUpdate()
                            .Subscribe(_ =>
                            {
                                externalPaws[j].position = CalculateEllipse(externalTimes[j], externalFocus[j]);

                                externalTimes[j] += speed * Time.deltaTime;

                                if (externalLap[j] < 2)
                                {
                                    if (externalTimes[j] >= 2 * Mathf.PI)
                                    {
                                        externalTimes[j] = 0;
                                        externalLap[j]++;
                                        externalFocus[j] += externalDeltaOffset;
                                    }
                                }
                                else if (externalLap[j] < 3)
                                {
                                    if (externalTimes[j] >= 2 * Mathf.PI)
                                    {
                                        externalTimes[j] = 0;
                                        externalLap[j]++;
                                        externalFocus[j] -= externalDeltaOffset;
                                    }
                                }
                                else
                                {
                                    if (externalTimes[j] >= 1.75f * Mathf.PI + 2 * externalInterval * Mathf.Deg2Rad - externalInterval * Mathf.Deg2Rad * j)
                                    {
                                        externalDispose[j]?.Dispose();
                                        //externalDelayDispose[j]?.Dispose();
                                        //externalDelayDispose[j] = Observable.Timer(TimeSpan.FromSeconds(0.5f))
                                        Move(externalPaws[j], externalPaws[j].position, 0.5f, () =>
                                           Move(externalPaws[j], externalStartPos[j], delay, () =>
                                            {
                                                completeCount++;
                                                if (completeCount >= internalPaws.Length + externalPaws.Length)
                                                {
                                                    callback?.Invoke();
                                                }
                                            }));
                                    }
                                }
                            });
                    });
            }
        }

        private void Move(Transform target, Vector3 to, float time, Action complete)
        {
            float ElapsedTime = 0;
            Vector3 StartPosition = target.position;
            IDisposable dispose = null;
            dispose = Observable.EveryLateUpdate()
                .Subscribe(_ =>
                {
                    ElapsedTime += Time.deltaTime;
                    target.position = Vector3.Lerp(StartPosition, to, ElapsedTime / time);
                    if (ElapsedTime >= time)
                    {
                        moveDisposes.Remove(dispose);
                        dispose?.Dispose();
                        complete?.Invoke();
                    }
                });
            moveDisposes.Add(dispose);
        }

        public void Dispose()
        {
            foreach (var item in internalDispose)
            {
                item?.Dispose();
            }
            foreach (var item in externalDispose)
            {
                item?.Dispose();
            }
            foreach (var item in internalDelayDispose)
            {
                item?.Dispose();
            }
            foreach (var item in externalDelayDispose)
            {
                item?.Dispose();
            }
            foreach (var item in moveDisposes)
            {
                item?.Dispose();
            }
            moveDisposes.Clear();
        }

        private Vector3 CalculateEllipse(float angle, float focus)
        {
            Vector3 newPosition = Vector3.zero;
            newPosition.y = height * Mathf.Sin(angle);
            float a = Mathf.Sqrt(focus * focus + height * height);
            newPosition.x = pivot.position.x + rightOffset - a + a * Mathf.Cos(angle);
            return newPosition;
        }
    }
}
