using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameLogic;
using System;
using Zenject;
using UniRx;

namespace Kudos.GameObjects.Bosses
{
    public class MaskPhase_2_Behaviour : StateMachineBehaviour
    {
        WeightedRandom<BossAttack> randomAttack = new WeightedRandom<BossAttack>();
        Animator _animator;
        [Inject] Player _player;
        [Inject] MaskBoss _boss;
        [Inject] MaskBallAttack[] maskBallAttacks;
        [Inject] MaskPawsAttack maskPawsAttack;
        [Inject] MaskBelchAttack maskBelchAttack;
        [Inject] SpearSpawner spearSpawner;

        List<IDisposable> disboses;
        IDisposable spearDispose;
        List<Action> finishActions;

        private int nextIndex = -1;
        private BossAttack lastState;

        private float nextSpearSpawn;

        [Inject]
        public void Construct()
        {
            disboses = new List<IDisposable>();
            finishActions = new List<Action>();
            randomAttack.AddEntry(new BossAttack(0, Breath), 10, 2);
            randomAttack.AddEntry(new BossAttack(0, BallAttack), 20, 2);
            randomAttack.AddEntry(new BossAttack(1, BelchAttack), 30, 5);
            randomAttack.AddEntry(new BossAttack(2, PawsAttack), 20, 5);
            nextSpearSpawn = Time.time + UnityEngine.Random.Range(4, 10);
            _player.OnDied += Dispose;
            _boss.OnDeath += (x) => Dispose();
        }
        private void Awake()
        {

        }

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _animator = animator;
            if (nextIndex == -1)
            {
                lastState = randomAttack.GetRandom();
                lastState.handler?.Invoke(animator);
            }
            else
                animator.SetFloat("Attack", nextIndex);

            spearDispose?.Dispose();
            spearDispose = Observable.EveryLateUpdate()
                .Subscribe(_ =>
                {
                    if (nextSpearSpawn <= Time.time)
                    {
                        nextSpearSpawn = Time.time + UnityEngine.Random.Range(1, 4) * 3;
                        spearSpawner.SpawnHorizontal();
                    }
                });
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    
        //}

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _animator = animator;
            foreach (var item in disboses)
            {
                item.Dispose();
            }
            disboses.Clear();

            foreach (var item in finishActions)
            {
                item?.Invoke();
            }
            finishActions.Clear();

            //if (_boss.CurrentHP <= _boss.StartHP / 2)
            //{
            //    animator.SetTrigger("NextPhase");
            //}
            if (_boss.CurrentHP <= _boss.StartHP * 1 / 6)
            {
                animator.speed = 1.5f;
            }
            else if (_boss.CurrentHP <= _boss.StartHP * 2 / 6)
            {
                animator.speed = 1.25f;
            }

            spearDispose?.Dispose();
        }

        // OnStateMove is called right after Animator.OnAnimatorMove()
        //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that processes and affects root motion
        //}

        // OnStateIK is called right after Animator.OnAnimatorIK()
        //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that sets up animation IK (inverse kinematics)
        //}

        void Dispose()
        {
            foreach (var item in disboses)
            {
                item.Dispose();
            }
            disboses.Clear();
            spearDispose?.Dispose();
            foreach (var item in maskBallAttacks)
            {
                item.Dispose();
            }
            maskPawsAttack.Dispose();
            maskBelchAttack.Dispose();
            spearSpawner.Dispose();
            nextIndex = -1;
            if (_animator != null)
            {
                _animator.speed = 1;
                _animator.enabled = false;
            }
        }

        void Breath(Animator animator)
        {
            animator.SetFloat("Attack", 0);
        }

        void BallAttack(Animator animator)
        {
            animator.SetFloat("Attack", 0);
            nextIndex = 0;
            int repeatCount = 2;
            List<int> pool = new List<int>();
            List<int> queue = new List<int>();
            for (int i = 0; i < maskBallAttacks.Length; i++)
            {
                pool.Add(i);
            }
            for (int i = repeatCount - 1; i >= 0; i--)
            {
                int rand = UnityEngine.Random.Range(0, pool.Count);
                queue.Add(rand);
                pool.RemoveAt(rand);
            }

            for (int i = 0; i < queue.Count; i++)
            {
                int ii = i;
                Observable.Timer(TimeSpan.FromSeconds(UnityEngine.Random.Range(0.1f, 0.6f)))
                   .Subscribe(_ =>
                   {
                       maskBallAttacks[queue[ii]].ReInit(null);
                       if (nextIndex == 0)
                           nextIndex = -1;
                   });
            }
        }

        void BelchAttack(Animator animator)
        {
            animator.SetFloat("Attack", 1);
            IDisposable disposable = Observable.Timer(TimeSpan.FromSeconds(0.5))
                .Subscribe(_=> maskBelchAttack.ReInit());
            disboses.Add(disposable);
            
            nextIndex = -1;

        }

        void PawsAttack(Animator animator)
        {
            animator.SetFloat("Attack", 2);
            nextIndex = 2;
            maskPawsAttack.ReInit(() => { nextIndex = -1; });
        }
    }
}
