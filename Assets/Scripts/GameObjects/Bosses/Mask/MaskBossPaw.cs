using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameLogic;

namespace Kudos.GameObjects.Bosses
{
    public class MaskBossPaw : MonoBehaviour
    {
        Collider _collider;

        public bool IsEnableTrigger { get; private set; }

        private void Awake()
        {
            _collider = GetComponent<Collider>();
            IsEnableTrigger = _collider.enabled;
        }
        private void OnTriggerEnter(Collider other)
        {
            Player player = other.GetComponent<Player>();
            if (IsEnableTrigger && player != null && !player.IsInvincible && player.IsAlive)
            {
                player.TakeDamage(name);
            }
        }

        public void SetActiveCollides(bool value)
        {
            IsEnableTrigger = value;
            _collider.enabled = value;
        }
    }
}
