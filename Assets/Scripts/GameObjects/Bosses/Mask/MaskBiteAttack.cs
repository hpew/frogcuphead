using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using Zenject;
using Kudos.GameLogic;

namespace Kudos.GameObjects.Bosses
{
    public class MaskBiteAttack : MonoBehaviour, IInitializable, IDisposable
    {
        [SerializeField]
        private Renderer[] bossMaterial;
        [SerializeField]
        private Renderer[] eyesMaterial;

        [SerializeField]
        private float speed;

        [Inject] MaskBoss _boss;
        [Inject] Player _player;
        [Inject] MaskBossPaw[] _paws;

        private Color startBossColor;
        private Vector3 startBossPos;

        IDisposable moveDispose;
        IDisposable timerDispose;
        LTDescr backDispose;

        public void Initialize()
        {
            startBossColor = _boss.BossDamageEffect.StartColor;
            startBossPos = _boss.transform.position;
            //gameObject.SetActive(false);
            foreach (var item in eyesMaterial)
            {
                item.material.color = startBossColor;
            }
        }

        public void ReInit(Action callback)
        {
            backDispose?.pause();
            _boss.SetActiveCollides(false);
            foreach (var item in _paws)
            {
                item.SetActiveCollides(false);
            }
            foreach (var item in bossMaterial)
            {
                item.material.color = Color.clear;
            }
            //_boss.transform.position = startBossPos;
            //gameObject.SetActive(true);
            foreach (var item in eyesMaterial)
            {
                item.material.color = Color.green;
            }

            timerDispose?.Dispose();
            moveDispose?.Dispose();
            moveDispose = Observable.EveryLateUpdate()
                .Subscribe(_ => Move());
            timerDispose = Observable.Timer(TimeSpan.FromSeconds(UnityEngine.Random.Range(1, 4)))
                .Subscribe(_ =>
                {
                    moveDispose?.Dispose();
                    foreach (var item in eyesMaterial)
                    {
                        item.material.color = Color.red;
                    }
                    timerDispose = Observable.Timer(TimeSpan.FromSeconds(.5f))
                       .Subscribe(_ =>
                       {
                           foreach (var item in bossMaterial)
                           {
                               item.material.color = startBossColor;
                           }
                           //_boss.transform.position = startBossPos;
                           _boss.SetActiveCollides(true);
                           foreach (var item in _paws)
                           {
                               item.SetActiveCollides(true);
                           }
                           //gameObject.SetActive(false);           
                           callback?.Invoke();
                           timerDispose = Observable.Timer(TimeSpan.FromSeconds(.5f))
                               .Subscribe(_ =>
                               {
                                   foreach (var item in eyesMaterial)
                                   {
                                       item.material.color = startBossColor;
                                   }
                                   backDispose = LeanTween.move(_boss.gameObject, startBossPos, 1f);
                               });
                       });
                });
        }

        public void Dispose()
        {

            timerDispose?.Dispose();
            moveDispose?.Dispose();
            backDispose?.pause();
            if (_boss != null)
            {
                _boss.transform.position = startBossPos;
                _boss.SetActiveCollides(true);
                foreach (var item in _paws)
                {
                    item.SetActiveCollides(true);
                }
            }
            //foreach (var item in bossMaterial)
            //{
            //    item.material.color = startBossColor;
            //}
            //foreach (var item in eyesMaterial)
            //{
            //    item.material.color = startBossColor;
            //}
        }

        private void Move()
        {
            foreach (var item in bossMaterial)
            {
                item.material.color = Color.clear;
            }
            foreach (var item in eyesMaterial)
            {
                item.material.color = Color.green;
            }
            _boss.transform.position = Vector3.MoveTowards(_boss.transform.position, _player.transform.position + Vector3.down * 0.5f, Time.deltaTime * speed);
        }
    }
}
