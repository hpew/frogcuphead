using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using Zenject;
using Kudos.GameLogic;

namespace Kudos.GameObjects.Bosses
{
    public class MaskBelchAttack : MonoBehaviour, IDisposable
    {
        [Inject] AutoAimEnemy.Factory _factory;

        List<Enemy> enemies;
        private void Awake()
        {
            enemies = new List<Enemy>();
        }
        public void ReInit()
        {
            Enemy enemy = _factory.Create();
            enemy.transform.position = transform.position;
            enemy.transform.localScale = Vector3.one * 0.5f;
            enemy.Rebuild();
            enemy.StartExecution();
            enemy.OnDeath += DestoryEnemy;
            enemies.Add(enemy);
        }

        private void DestoryEnemy(Enemy enemy)
        {
            enemies.Remove(enemy);
            Destroy(enemy.gameObject);
        }

        public void Dispose()
        {
            for (int i = enemies.Count-1; i >= 0; i--)
            {
                enemies[i].Die();
            }
        }
    }
}
