using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kudos.GameLogic;
using System;
using Zenject;
using UniRx;

namespace Kudos.GameObjects.Bosses
{
    struct BossAttack
    {
        public int animationIndex;
        public Action<Animator> handler;

        public BossAttack(int animationIndex, Action<Animator> handler)
        {
            this.animationIndex = animationIndex;
            this.handler = handler;
        }
    }
    public class MaskPhase_1_Behaviour : StateMachineBehaviour
    {
        WeightedRandom<BossAttack> randomAttack = new WeightedRandom<BossAttack>();
        Animator _animator;
        [Inject] Player _player;
        [Inject] MaskBoss _boss;
        [Inject] MaskBallAttack[] maskBallAttacks;
        [Inject] MaskBiteAttack maskBiteAttack;
        [Inject] MaskSpreadGun maskSpreadGun;
        [Inject] SpearSpawner spearSpawner;

        List<IDisposable> disboses;
        List<Action> finishActions;

        private int nextIndex = -1;
        private BossAttack lastState;

        private float nextSpearSpawn;

        IDisposable spearDispose;

        [Inject]
        public void Construct()
        {
            disboses = new List<IDisposable>();
            finishActions = new List<Action>();
            randomAttack.AddEntry(new BossAttack(0, Breath), 10, 2);
            randomAttack.AddEntry(new BossAttack(0, BallAttack), 20, 2);
            randomAttack.AddEntry(new BossAttack(1, BiteAttack), 20, 5);
            randomAttack.AddEntry(new BossAttack(2, FearAttack), 20, 2);
            nextSpearSpawn = Time.time + UnityEngine.Random.Range(3, 6);
            _player.OnDied += Dispose;
            _boss.OnDeath += (x) => Dispose();
        }

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _animator = animator;
            if (nextIndex == -1)
            {
                lastState = randomAttack.GetRandom();
                lastState.handler?.Invoke(animator);
            }
            else
                animator.SetFloat("Attack", nextIndex);

            spearDispose?.Dispose();
            spearDispose = Observable.EveryLateUpdate()
                .Subscribe(_=> 
                {
                    if (nextSpearSpawn <= Time.time)
                    {
                        nextSpearSpawn = Time.time + UnityEngine.Random.Range(1, 5) * 2;
                        spearSpawner.SpawnRotate();
                    }
                });
        }



        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
           
        //}

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _animator = animator;
            foreach (var item in disboses)
            {
                item.Dispose();
            }
            disboses.Clear();

            foreach (var item in finishActions)
            {
                item?.Invoke();
            }
            finishActions.Clear();

            if (_boss.CurrentHP <= _boss.StartHP / 2)
            {
                animator.speed = 1;
                animator.SetTrigger("NextPhase");
            }
            else if (_boss.CurrentHP <= _boss.StartHP * 4 / 6)
            {
                animator.speed = 1.5f;
            }
            else if (_boss.CurrentHP <= _boss.StartHP * 5 / 6)
            {
                animator.speed = 1.25f;
            }

            spearDispose?.Dispose();
        }

        void Dispose()
        {
            foreach (var item in disboses)
            {
                item.Dispose();
            }
            disboses.Clear();
            spearDispose?.Dispose();
            foreach (var item in maskBallAttacks)
            {
                item.Dispose();
            }
            maskBiteAttack.Dispose();
            maskSpreadGun.Dispose();
            spearSpawner.Dispose();
            nextIndex = -1;
            if (_animator != null)
            {
                _animator.speed = 1;
                _animator.enabled = false;
            }
        }

        // OnStateMove is called right after Animator.OnAnimatorMove()
        //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that processes and affects root motion
        //}

        // OnStateIK is called right after Animator.OnAnimatorIK()
        //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that sets up animation IK (inverse kinematics)
        //}

        void Breath(Animator animator)
        {
            animator.SetFloat("Attack", 0);
        }

        void BallAttack(Animator animator)
        {
            animator.SetFloat("Attack", 0);
            nextIndex = 0;
            //disboses.Add(maskBallAttacks[0]);
            List<MaskBallAttack> ballAttacks = new List<MaskBallAttack>();
            foreach (var item in maskBallAttacks)
            {
                if (item.phase == 1)
                {
                    ballAttacks.Add(item);
                }
            }
            ballAttacks[UnityEngine.Random.Range(0, ballAttacks.Count)].ReInit(() => { nextIndex = -1; });
        }

        void BiteAttack(Animator animator)
        {
            animator.SetFloat("Attack", 1);
            float startSpeed = animator.speed;
            animator.speed = 0;
            maskBiteAttack.ReInit(() =>
            {
                animator.speed = startSpeed;
                nextIndex = -1;
            });
        }

        void FearAttack(Animator animator)
        {
            animator.SetFloat("Attack", 2);
            maskSpreadGun.StartShoot(_boss);
            finishActions.Add(() => { maskSpreadGun.StopShoot(); });
        }
    }
}
