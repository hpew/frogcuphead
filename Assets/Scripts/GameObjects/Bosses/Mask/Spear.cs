using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;
using Zenject;
using Kudos.GameLogic;

namespace Kudos.GameObjects.Bosses
{
    public class Spear : Obstacle, IDisposable
    {
        [Inject] Player _player;
        IDisposable startDispose;
        IDisposable moveDispose;
        IDisposable timeDispose;
        IDisposable lerpDispose;
        IDisposable translateDispose;
        [SerializeField]
        float lifeTime = 5;
        [SerializeField]
        float speed = 5;
        float observableTime = 0;

        public event Action<Spear> OnEndLifeTime;

        private void MoveLerp(Transform target, Vector3 to, float time, Action complete)
        {
            float ElapsedTime = 0;
            Vector3 StartPosition = target.position;
            lerpDispose = Observable.EveryLateUpdate()
                .Subscribe(_ =>
                {
                    ElapsedTime += Time.deltaTime;
                    target.position = Vector3.Lerp(StartPosition, to, ElapsedTime / time);
                    if (ElapsedTime >= time)
                    {
                        lerpDispose?.Dispose();
                        complete?.Invoke();
                    }
                });
        }
        private void MoveTranslate(Transform target, Vector3 direction, float time, Action complete)
        {
            float ElapsedTime = 0;
            Vector3 StartPosition = target.position;
            translateDispose = Observable.EveryLateUpdate()
                .Subscribe(_ =>
                {
                    ElapsedTime += Time.deltaTime;
                    target.Translate(direction * Time.deltaTime);
                    //target.position = Vector3.Lerp(StartPosition, to, ElapsedTime / time);
                    if (ElapsedTime >= time)
                    {
                        translateDispose?.Dispose();
                        complete?.Invoke();
                    }
                });
        }
        public void RotateObserveStart(Vector3 spawnPoint, Vector3 offset, float minObserveTime, float maxObserveTime)
        {
            transform.position = spawnPoint + offset;
            MoveLerp(transform, spawnPoint, 0.5f, () =>
            {
                RotateObserve(minObserveTime, maxObserveTime);
            });
        }

        private void RotateObserve(float minObserveTime, float maxObserveTime)
        {
            observableTime = 0;
            float observeTime = UnityEngine.Random.Range(minObserveTime, maxObserveTime);
            startDispose = Observable.EveryLateUpdate()
               .Subscribe(_ =>
               {
                   if (observableTime >= observeTime)
                   {
                       startDispose?.Dispose();
                       MoveTranslate(transform, Vector3.right, 0.25f, StartMoving);
                   }
                   Vector3 dir = _player.transform.position - transform.position;
                   float angle = Vector3.Angle(dir, Vector3.down);
                   transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0,0, 90-angle), Time.deltaTime * 360);
                   observableTime += Time.deltaTime;
               });
        }

        public void HorizontalObserveStart(Vector3 spawnPoint, Vector3 offset, float minObserveTime, float maxObserveTime)
        {
            transform.position = spawnPoint + offset;
            MoveLerp(transform, spawnPoint, 0.5f, () =>
            {
                HorizontalObserve(minObserveTime, maxObserveTime);
            });
        }

        private void HorizontalObserve(float minObserveTime, float maxObserveTime)
        {
            observableTime = 0;
            float observeTime = UnityEngine.Random.Range(minObserveTime, maxObserveTime) * 1.5f;
            startDispose = Observable.EveryLateUpdate()
               .Subscribe(_ =>
               {
                   if (observableTime >= observeTime)
                   {
                       startDispose?.Dispose();
                       MoveTranslate(transform, Vector3.right, 0.25f, StartMoving);
                   }
                   Vector3 dir = new Vector3(_player.transform.position.x, transform.position.y, transform.position.z);
                   transform.position = Vector3.MoveTowards(transform.position, dir, speed/2 * Time.deltaTime);
                   observableTime += Time.deltaTime;
               });
        }

        public void Dispose()
        {
            startDispose?.Dispose();
            timeDispose?.Dispose();
            moveDispose?.Dispose();
            lerpDispose?.Dispose();
            translateDispose?.Dispose();
        }

        private void StartMoving()
        {
            moveDispose?.Dispose();
            moveDispose = Observable.EveryLateUpdate()
                .Subscribe(_ => Move(Vector3.left));

            timeDispose?.Dispose();
            timeDispose = Observable.Timer(TimeSpan.FromSeconds(lifeTime))
                .Subscribe(_ =>
                {
                    Dispose();
                    OnEndLifeTime?.Invoke(this);
                });
        }

        private void Move(Vector3 velocity)
        {
            transform.Translate(velocity * speed * Time.deltaTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            Player player = other.GetComponent<Player>();
            if (player != null && !player.IsInvincible && player.IsAlive)
            {
                player.TakeDamage(name);
            }
        }

        public class Pool : MonoMemoryPool<Vector3, Quaternion, Spear>, IValidatable
        {
            protected override void OnCreated(Spear spear)
            {
                base.OnCreated(spear);
                spear.name = "Spear";
            }
            protected override void Reinitialize(Vector3 position, Quaternion rotation, Spear spear)
            {
                spear.transform.position = position;
                spear.transform.rotation = rotation;
            }

            protected override void OnDespawned(Spear spear)
            {
                spear.OnEndLifeTime = null;
                spear.Dispose();
                base.OnDespawned(spear);

            }
        }
    }
}
