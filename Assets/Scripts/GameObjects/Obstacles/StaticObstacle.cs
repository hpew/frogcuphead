using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.GameObjects
{
    public class StaticObstacle : Obstacle
    {
        private void OnTriggerStay(Collider other)
        {
            Player target = other.GetComponent<Player>();
            if (target != null)
            {
                if (target.IsIgnored()) return;
                target.TakeDamage("Obstacle", 1);
            }
        }
    }
}
