using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Kudos.GameObjects
{
    public class MovingObstacle : Obstacle
    {
        [SerializeField]
        private Transform targetA;
        [SerializeField]
        private Transform targetB;

        public float speed = 1;

        public Vector3 PositionA { get; private set; }
        public Vector3 PositionB { get; private set; }
        
        private Vector3 target;
        
        private IDisposable moveDispose;

        private void Start()
        {
            PositionA = targetA.position;
            PositionB = targetB.position;

            target = PositionA;
            moveDispose = Observable.EveryUpdate()
                .Subscribe(_=> 
                {
                    if ((target - transform.position).magnitude < 0.01f)
                    {
                        if (target == PositionA)
                            target = PositionB;
                        else
                            target = PositionA;
                    }

                    transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
                    targetA.position = PositionA;
                    targetB.position = PositionB;
                });
        }

        private void OnDestroy()
        {
            moveDispose?.Dispose();
        }

        private void OnTriggerStay(Collider other)
        {
            Player target = other.GetComponent<Player>();
            if (target != null)
            {
                if (target.IsIgnored()) return;
                target.TakeDamage(name, 1);
            }
        }
    }
}
