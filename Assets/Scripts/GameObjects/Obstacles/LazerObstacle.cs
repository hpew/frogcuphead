using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Kudos.ShadersAPI;

namespace Kudos.GameObjects
{
    public class LazerObstacle : Obstacle, ILockable
    {
        private LazerControll lazer;
        private Collider _collider;
        private void Start()
        {
            lazer = GetComponentInChildren<LazerControll>(true);
            _collider = GetComponent<Collider>();
        }
        public void Init(bool value)
        {
            lazer.gameObject.SetActive(value);
            _collider.enabled = value;
        }

        public void Lock(bool value)
        {
            lazer.gameObject.SetActive(value);
            _collider.enabled = value;
        }
    }
}
