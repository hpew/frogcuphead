using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.GameObjects
{
    public interface ILockable
    {
        public void Init(bool value);
        public void Lock(bool value);
    }

    public interface IActivatable
    {
        public void Activate();
    }
}
