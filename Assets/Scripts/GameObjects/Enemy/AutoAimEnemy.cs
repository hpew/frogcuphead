using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;
using UniRx;
using Kudos.ShadersAPI;

namespace Kudos.GameObjects
{
    public class AutoAimEnemy : Enemy
    {
        [Inject] Player player;

        [SerializeField]
        float smoothTime = 0.5f;
        Vector3 velocity = Vector3.zero;

        IDisposable moveDispose;

        protected override void RebuildSelf()
        {

        }

        protected override void StartExecutionSelf()
        {
            moveDispose?.Dispose();
            moveDispose = Observable.EveryLateUpdate()
                .Subscribe(_ =>
                {
                    Move();
                });
        }
        protected override void FinishExecutionSelf()
        {
            moveDispose?.Dispose();
        }

        protected override void PrepareToHit()
        {
            base.PrepareToHit();
        }

        protected override void PrepareToDie()
        {

        }

        private void Move()
        {
            //transform.position = Vector3.SmoothDamp(transform.position, player.transform.position, ref velocity, smoothTime * Time.deltaTime);
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, smoothTime * Time.deltaTime);
            transform.LookAt(player.transform);
        }

        private void OnTriggerEnter(Collider other)
        {
            Player player = other.GetComponent<Player>();
            if (player != null && !player.IsInvincible && player.IsAlive)
            {
                player.TakeDamage(name);
                Die();
            }
        }

        protected override void AfterDie()
        {
            //Destroy(gameObject);
        }

        public class Factory : PlaceholderFactory<Enemy>
        {
        }
    }
}
