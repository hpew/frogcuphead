using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;
using UniRx;
using UniRx.Triggers;

namespace Kudos.GameObjects
{
    public class MovingEnemy : Enemy
    {
        [Inject] Player player;

        public float activateDistance = 3f;
        public float attackDelay = 2f;
        public float speed = 1;
        public Vector3 direction = Vector3.left;

        //public Animator _animator;
        public bool IsAttacking { get; private set; }

        private Vector3 startPos;
        IDisposable moveDispose;
        IDisposable attakingDispose;

        private void Start()
        {
            
        }

        protected override void RebuildSelf()
        {
            startPos = transform.position;
            IsAttacking = false;
        }

        protected override void StartExecutionSelf()
        {
            transform.position = startPos;
            moveDispose?.Dispose();
            moveDispose = Observable.EveryLateUpdate()
                .Subscribe(_ =>
                {
                    if (transform.position.x - player.transform.position.x <= activateDistance)
                    {
                        transform.position = new Vector3(player.transform.position.x + activateDistance, transform.position.y, transform.position.z);
                        if (!IsAttacking)
                        {
                            IsAttacking = true;
                            AttackDelay();
                        }
                    }
                });
        }
        protected override void FinishExecutionSelf()
        {
            moveDispose?.Dispose();
            attakingDispose?.Dispose();
        }

        protected override void PrepareToHit()
        {
            base.PrepareToHit();
        }

        protected override void PrepareToDie()
        {

        }

        private void AttackDelay()
        {
            attakingDispose?.Dispose();
            attakingDispose = Observable.Timer(TimeSpan.FromSeconds(attackDelay))
                .Subscribe(_ =>
                {
                    Attack();
                });
        }

        private void Attack()
        {
            _animator.SetTrigger("Attack");
            var stateMachive = _animator.GetBehaviour<ObservableStateMachineTrigger>();
            stateMachive.OnStateExitAsObservable()
                .Where(b => b.StateInfo.IsName("Attacking"))
                .Subscribe(_ =>
                {
                    //attakingDispose?.Dispose();
                    //moveDispose?.Dispose();
                    StartMoving();
                });
        }

        private void StartMoving()
        {
            attakingDispose?.Dispose();
            moveDispose?.Dispose();
            moveDispose = Observable.EveryLateUpdate()
                .Subscribe(_ =>
                {
                    transform.Translate(direction * speed * Time.deltaTime);
                    if (player.transform.position.x - transform.position.x >= activateDistance)
                    {
                        moveDispose?.Dispose();
                    }
                });
        }
    }
}
