using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;
using UniRx;
using UniRx.Triggers;

namespace Kudos.GameObjects
{
    public class WalkingEnemy : Enemy
    {
        [Inject] RunPlayer player;

        public float activateDistance = 3f;
        public float speed = 10;
        public Vector3 direction = Vector3.left;

        //public Animator _animator;

        private Vector3 startPos;
        IDisposable moveDispose;

        private void Start()
        {
            startPos = transform.position;
        }

        protected override void RebuildSelf()
        {
            transform.position = startPos;
        }

        protected override void StartExecutionSelf()
        {
            moveDispose?.Dispose();
            moveDispose = Observable.EveryLateUpdate()
                .Subscribe(_ =>
                {
                    if (transform.position.x - player.transform.position.x <= activateDistance)
                    {
                        StartMoving();
                    }
                });
        }
        protected override void FinishExecutionSelf()
        {
            moveDispose?.Dispose();
        }

        protected override void PrepareToHit()
        {
            base.PrepareToHit();
        }

        protected override void PrepareToDie()
        {

        }

        private void StartMoving()
        {
            moveDispose?.Dispose();
            moveDispose = Observable.EveryLateUpdate()
                .Subscribe(_ =>
                {
                    transform.Translate(direction * speed * Time.deltaTime);
                    if (player.transform.position.x - transform.position.x >= activateDistance)
                    {
                        moveDispose?.Dispose();
                    }
                });
        }
    }
}
