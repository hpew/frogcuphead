using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.GameObjects
{
    public class ShootingEnemy : Enemy
    {
        public Gun gun;

        protected override void RebuildSelf()
        {
            gun.Dispose();
        }

        protected override void StartExecutionSelf()
        {
            gun.StartShoot(this);
        }
        protected override void FinishExecutionSelf()
        {
            gun.StopShoot();
        }

        protected override void PrepareToHit()
        {
            base.PrepareToHit();
        }

        protected override void PrepareToDie()
        {

        }
    }
}
