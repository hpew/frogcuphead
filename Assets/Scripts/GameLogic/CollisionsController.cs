using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kudos.GameLogic
{
    public interface IIgnorable { }
    public enum CollisionSideType { none, above, below, left, right }
    public struct CollisionInfo
    {
        public CollisionSideType collisionSide;
        public Collider collider;
        public Vector3 point;
        public bool isCoyotesTime;
        public void Reset()
        {
            collisionSide = CollisionSideType.none;
            isCoyotesTime = false;
            collider = null;
            point = Vector3.zero;
        }
    }
    public class CollisionsController
    {
        private Collider _collider;
        private RaycastOrigins raycastOrigins;
        private CollisionInfo collisions;

        const float skinWidth = .015f;
        private int horizontalRayCount = 4;
        private int verticalRayCount = 4;

        private float horizontalRaySpacing;
        private float verticalRaySpacing;

        private bool useCoyotesTime;

        private bool isCoyotesTime = false;

        public CollisionInfo Collisions => collisions;

        public event System.Action<CollisionInfo> OnCollision;

        public bool IsIgnore { get; set; }

        public CollisionsController(Collider collider, bool useCoyotesTime = true)
        {
            this.useCoyotesTime = useCoyotesTime;
            _collider = collider;
            CalculateRaySpacing();
        }

        public CollisionsController(Collider collider, bool useCoyotesTime, int horizontalRayCount, int verticalRayCount)
        {
            this.useCoyotesTime = useCoyotesTime;
            this.horizontalRayCount = horizontalRayCount;
            this.verticalRayCount = verticalRayCount;
            _collider = collider;
            CalculateRaySpacing();
        }

        public Vector3 Move(Vector3 velocity)
        {
            UpdateRaycastOrigins();
            collisions.Reset();
            if (velocity.x != 0)
            {
                HorizontalCollisions(ref velocity);
            }
            if (velocity.y != 0)
            {
                VerticalCollisions(ref velocity);
            }
            if (useCoyotesTime && collisions.collisionSide == CollisionSideType.none && isCoyotesTime)
            {
                CoyotesTimeCollisions(ref velocity);
            }
            return velocity;
        }

        void HorizontalCollisions(ref Vector3 velocity)
        {
            float directionX = Mathf.Sign(velocity.x);
            float rayLength = Mathf.Abs(velocity.x) + skinWidth;

            for (int i = 0; i < horizontalRayCount; i++)
            {
                Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
                rayOrigin += Vector2.up * (horizontalRaySpacing * i);
                RaycastHit hit;
                if (Physics.Raycast(rayOrigin, Vector2.right * directionX, out hit, rayLength))
                {
                    if (IsIgnore && hit.collider.GetComponent<GameObjects.Obstacle>() != null)
                    {
                        continue;
                    }
                    collisions.collisionSide = directionX == -1 ? CollisionSideType.left : CollisionSideType.right;
                    collisions.point = hit.point;
                    collisions.collider = hit.collider;
                    OnCollision?.Invoke(collisions);
                    if (hit.collider.GetComponent<IIgnorable>() == null)
                    {
                        velocity.x = (hit.distance - skinWidth) * directionX;
                        rayLength = hit.distance;
                    }
                    else
                    {
                        collisions.collisionSide = CollisionSideType.none;
                    }

                }
                Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);
            }
        }

        void VerticalCollisions(ref Vector3 velocity)
        {
            float directionY = Mathf.Sign(velocity.y);
            float rayLength = Mathf.Abs(velocity.y) + skinWidth;
            for (int i = 0; i < verticalRayCount; i++)
            {
                Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += Vector2.right * (verticalRaySpacing * i + velocity.x);
                RaycastHit hit;
                if (Physics.Raycast(rayOrigin, Vector2.up * directionY, out hit, rayLength))
                {
                    if (IsIgnore && hit.collider.GetComponent<GameObjects.Obstacle>() != null)
                    {
                        continue;
                    }
                    if (hit.collider.GetComponent<IIgnorable>() == null)
                    {
                        velocity.y = (hit.distance - skinWidth) * directionY;
                        rayLength = hit.distance;
                        collisions.collisionSide = directionY == -1 ? CollisionSideType.below : CollisionSideType.above;
                        isCoyotesTime = true;
                    }
                    collisions.collider = hit.collider;
                    collisions.point = hit.point;
                    OnCollision?.Invoke(collisions);
                }
                Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.yellow);
            }

        }

        void CoyotesTimeCollisions(ref Vector3 velocity)
        {
            isCoyotesTime = false;
            float directionY = Mathf.Sign(velocity.y);
            float directionX = Mathf.Sign(velocity.x);
            float rayLength = Mathf.Abs(velocity.y) + skinWidth;
            for (int i = 0; i < verticalRayCount; i++)
            {
                Vector2 rayOrigin = Vector2.zero;
                if (directionX == -1)
                    rayOrigin = (directionY == -1) ? raycastOrigins.bottomRight : raycastOrigins.topRight;
                else
                    rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += -directionX * Vector2.right * (verticalRaySpacing * i + velocity.x);
                RaycastHit hit;
                if (Physics.Raycast(rayOrigin, Vector2.up * directionY, out hit, rayLength))
                {
                    if (hit.collider.GetComponent<IIgnorable>() == null && hit.collider.GetComponent<GameObjects.Obstacle>() == null)
                    {
                        velocity.y = (hit.distance - skinWidth) * directionY;
                        rayLength = hit.distance;
                        collisions.collisionSide = directionY == -1 ? CollisionSideType.below : CollisionSideType.above;
                        collisions.collider = hit.collider;
                        collisions.point = hit.point;
                        collisions.isCoyotesTime = true;
                        OnCollision?.Invoke(collisions);
                        isCoyotesTime = true;
                    }
                }
                Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.cyan);
            }
        }

        void UpdateRaycastOrigins()
        {
            Bounds bounds = _collider.bounds;
            bounds.Expand(skinWidth * -2);

            raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
            raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
            raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
        }

        void CalculateRaySpacing()
        {
            Bounds bounds = _collider.bounds;
            bounds.Expand(skinWidth * -2);

            horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);
            verticalRayCount = Mathf.Clamp(verticalRayCount, 2, int.MaxValue);

            horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
            verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
        }

        struct RaycastOrigins
        {
            public Vector2 topLeft, topRight;
            public Vector2 bottomLeft, bottomRight;
        }
    }
}
