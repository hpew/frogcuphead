using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace Kudos.GameLogic
{
    public enum InputType
    {
        none,
        tap,
        hold,
        downSwipe,
        upSwipe,
        rightSwipe,
        leftSwipe,
    }

    public class InputInfo
    {
        public InputType inputType;
        public InputType lastInputType;
        public Vector3 touchPosition;
        public float holdTime;
        public bool isTouch;
        public int touchIndex;

        public bool IsHold()
        {
            if (inputType >= InputType.hold)
            {
                return true;
            }
            return false;
        }
    }

    public class InputSystem : IDisposable
    {
        public float MAX_HOLD_TIME = 0.1f;
        public float MIN_SWIPE_DISTANCE = 0.05f;
        IDisposable dispose;

        float emulateHoldTime;
        Vector2 startPos;
        Vector2 swipe;

        float[] TouchesHoldTime;
        Vector2[] TouchesStartPos;
        Vector2[] TouchesSwipe;
        List<InputInfo> info;
        //public static InputInfo InputInfo { get; private set; }
        public static event Action<InputInfo[]> OnInputUpdate;
        public static event Action OnMouseButtonDown;

        public InputSystem()
        {
            //InputInfo = new InputInfo();
            info = new List<InputInfo>();
            TouchesHoldTime = new float[10];
            TouchesStartPos = new Vector2[10];
            TouchesSwipe = new Vector2[10];
            dispose = Observable.EveryUpdate()
                .Subscribe(_ => Update());
        }

        private void Update()
        {
            info.Clear();
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                //InputInfo.lastInputType = InputInfo.inputType;
                InputInfo InputInfo = new InputInfo();
                InputInfo.inputType = InputType.none;
                InputInfo.touchPosition = Input.mousePosition;
                InputInfo.holdTime = 0;
                InputInfo.touchIndex = 0;
                if (Input.GetMouseButtonDown(0))// && Input.mousePosition.x < Screen.width / 2)
                {
                    OnMouseButtonDown?.Invoke();
                    InputInfo.isTouch = true;
                    emulateHoldTime = 0;
                    startPos = new Vector2(Input.mousePosition.x / (float)Screen.width, Input.mousePosition.y / (float)Screen.width);
                }
                if (Input.GetMouseButton(0))// && Input.mousePosition.x < Screen.width / 2)
                {
                    InputInfo.holdTime = emulateHoldTime;
                    Vector2 endPos = new Vector2(Input.mousePosition.x / (float)Screen.width, Input.mousePosition.y / (float)Screen.width);
                    swipe = new Vector2(endPos.x - startPos.x, endPos.y - startPos.y);

                    if (swipe.magnitude < MIN_SWIPE_DISTANCE)
                    {   //Hold
                        //if (holdTime >= MAX_HOLD_TIME)
                        InputInfo.inputType = InputType.hold;
                    }
                    else
                    {   // Swipe
                        if (Mathf.Abs(swipe.x) > Mathf.Abs(swipe.y))
                        {   // Horizontal swipe
                            if (swipe.x > 0)
                            {
                                InputInfo.inputType = InputType.rightSwipe;
                            }
                            else
                            {
                                InputInfo.inputType = InputType.leftSwipe;
                            }
                        }
                        else
                        {   // Vertical swipe
                            if (swipe.y > 0)
                            {
                                InputInfo.inputType = InputType.upSwipe;
                            }
                            else
                            {
                                InputInfo.inputType = InputType.downSwipe;
                            }
                        }
                        startPos = new Vector2(Input.mousePosition.x / (float)Screen.width, Input.mousePosition.y / (float)Screen.width);
                    }
                }
                if (Input.GetMouseButtonUp(0))// && Input.mousePosition.x < Screen.width / 2)
                {
                    InputInfo.isTouch = false;
                    if (emulateHoldTime < MAX_HOLD_TIME)
                    {   // Hold or Swipe
                        InputInfo.holdTime = emulateHoldTime;
                        InputInfo.inputType = InputType.tap;
                    }
                }
                emulateHoldTime += Time.deltaTime;
                info.Add(InputInfo);
            }
            else
            {
                for (int i = 0; i < Mathf.Clamp(Input.touchCount, 0, 10); i++)
                {
                    Touch touch = Input.GetTouch(i);
                    //InputInfo.lastInputType = InputInfo.inputType;
                    InputInfo InputInfo = new InputInfo();
                    InputInfo.inputType = InputType.none;
                    InputInfo.touchPosition = touch.position;
                    InputInfo.holdTime = 0;
                    InputInfo.touchIndex = i;
                    //touch.fingerId
                    if (touch.phase == TouchPhase.Began)
                    {
                        OnMouseButtonDown?.Invoke();
                        InputInfo.isTouch = true;
                        TouchesHoldTime[i] = 0;
                        TouchesStartPos[i] = new Vector2(touch.position.x / (float)Screen.width, touch.position.y / (float)Screen.width);
                    }
                    if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                    {

                        DebugText.Log("index " + i + " Id " + touch.fingerId);
                        InputInfo.holdTime = TouchesHoldTime[i];
                        Vector2 endPos = new Vector2(touch.position.x / (float)Screen.width, touch.position.y / (float)Screen.width);
                        //DebugText.Log("start " + TouchesStartPos[i] + " end " + endPos);
                        TouchesSwipe[i] = new Vector2(endPos.x - TouchesStartPos[i].x, endPos.y - TouchesStartPos[i].y);
                        DebugText.Log("index " + i + "Swipe " + TouchesSwipe[i].magnitude);
                        if (TouchesSwipe[i].magnitude < MIN_SWIPE_DISTANCE)
                        {   //Hold
                            //if (holdTime >= MAX_HOLD_TIME)
                            InputInfo.inputType = InputType.hold;
                        }
                        else
                        {   // Swipe
                            if (Mathf.Abs(TouchesSwipe[i].x) > Mathf.Abs(TouchesSwipe[i].y))
                            {   // Horizontal swipe
                                if (TouchesSwipe[i].x > 0)
                                {
                                    InputInfo.inputType = InputType.rightSwipe;
                                }
                                else
                                {
                                    InputInfo.inputType = InputType.leftSwipe;
                                }
                            }
                            else
                            {   // Vertical swipe
                                if (TouchesSwipe[i].y > 0)
                                {
                                    InputInfo.inputType = InputType.upSwipe;
                                }
                                else
                                {
                                    InputInfo.inputType = InputType.downSwipe;
                                }
                            }
                            TouchesStartPos[i] = new Vector2(touch.position.x / (float)Screen.width, touch.position.y / (float)Screen.width);
                        }
                    }
                    if (touch.phase == TouchPhase.Ended)
                    {
                        TouchesStartPos[i] = Vector2.zero;
                        TouchesSwipe[i] = Vector2.zero;
                        InputInfo.isTouch = false;
                        if (TouchesHoldTime[i] < MAX_HOLD_TIME)
                        {   // Hold or Swipe
                            InputInfo.holdTime = TouchesHoldTime[i];
                            InputInfo.inputType = InputType.tap;
                        }
                    }
                    info.Add(InputInfo);
                    TouchesHoldTime[i] += Time.deltaTime;
                    //DebugText.Log("android: " + InputInfo.inputType.ToString());
                }
            }
            OnInputUpdate?.Invoke(info.ToArray());
            //DebugText.Log("editor: " + InputInfo.inputType.ToString());
        }

        public void Dispose()
        {
            OnInputUpdate = null;
            dispose?.Dispose();
        }
    }

    public class InputMessage
    {
        public InputInfo InputInfo { get; private set; }
        public InputMessage(InputInfo inputInfo)
        {
            InputInfo = inputInfo;
        }

        public static InputMessage Create(InputInfo inputInfo)
        {
            return new InputMessage(inputInfo);
        }
    }
}