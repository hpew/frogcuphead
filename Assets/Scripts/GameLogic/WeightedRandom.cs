﻿using System;
using System.Collections.Generic;

namespace Kudos.GameLogic
{
    public class WeightedRandom<T>
    {
        private class Entry
        {
            public double weight;
            public double accumulatedWeight;
            public double penaltyWeight;
            public T item;
        }

        private List<Entry> entries;
        private double accumulatedWeight;
        private Random rand;

        public WeightedRandom()
        {
            entries = new List<Entry>();
            accumulatedWeight = 0;
            rand = new Random();
        }

        public WeightedRandom(string seed)
        {
            entries = new List<Entry>();
            accumulatedWeight = 0;
            rand = new Random(seed.GetHashCode());
        }

        public void AddEntry(T item, double weight)
        {
            accumulatedWeight += weight;
            entries.Add(new Entry { weight = weight, item = item, accumulatedWeight = accumulatedWeight, penaltyWeight = 0 });
        }

        public void AddEntry(T item, double weight, double penalty)
        {
            accumulatedWeight += weight;
            entries.Add(new Entry { weight = weight, item = item, accumulatedWeight = accumulatedWeight, penaltyWeight = penalty });
        }

        public T GetRandom()
        {
            double r = rand.NextDouble() * accumulatedWeight;
            Entry resultEntry = null;

            foreach (Entry entry in entries)
            {
                if (entry.accumulatedWeight >= r)
                {
                    resultEntry = entry;
                    break;
                    //return entry.item;
                }
            }

            if (resultEntry == null) return default(T); //should only happen when there are no entries

            if (resultEntry.penaltyWeight != 0)
            {
                if (resultEntry.weight - resultEntry.weight >= 0)
                    resultEntry.weight -= resultEntry.penaltyWeight * 2;

                for (int i = 0; i < entries.Count; i++)
                {
                    entries[i].weight += resultEntry.penaltyWeight;
                }
                ReCalculate();
            }
            return resultEntry.item;
        }

        private void ReCalculate()
        {
            Entry[] temp = entries.ToArray();
            accumulatedWeight = 0;
            entries.Clear();

            foreach (var entry in temp)
            {
                AddEntry(entry.item, entry.weight, entry.penaltyWeight);
            }
        }
    }
}
