using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UniRx;

namespace Kudos.GameLogic
{
    public class StaminaController : IDisposable
    {
        private StaminaSlider _slider;

        private float startValue;

        public float CurrentValue { get; private set; }

        public float RegainValue { get; set; } = 1;

        public bool IsRegaining { get; set; } = true;

        IDisposable regainDispose;
        IDisposable delayDispose;

        public StaminaController(StaminaSlider slider)
        {
            _slider = slider;
        }

        public void ReInit(float startValue)
        {
            this.startValue = startValue;
            CurrentValue = startValue;
            _slider.Slider.minValue = 0;
            _slider.Slider.maxValue = startValue;
            _slider.Slider.value = CurrentValue;
            delayDispose?.Dispose();
            StartRegaining();
        }

        private void StartRegaining()
        {
            regainDispose?.Dispose();
            regainDispose = Observable.EveryUpdate()
                .Subscribe(_ => Regaining());
        }

        public bool SubstractStamina(float value)
        {
            if (CurrentValue - value > 0)
            {
                CurrentValue -= value;
                _slider.Slider.value = CurrentValue;
                return true;
            }
            else if (CurrentValue - value == 0)
            {
                CurrentValue -= value;
                _slider.Slider.value = CurrentValue;
                regainDispose?.Dispose();
                delayDispose = Observable.Timer(TimeSpan.FromSeconds(1))
                    .Subscribe(_ => StartRegaining());
                return true;
            }
            return false;
        }

        private void Regaining()
        {
            if (IsRegaining && CurrentValue != startValue)
            {
                CurrentValue += RegainValue;
                if (CurrentValue > startValue)
                    CurrentValue = startValue;
                _slider.Slider.value = CurrentValue;
            }
        }

        public void Dispose()
        {
            regainDispose?.Dispose();
            delayDispose?.Dispose();
        }
    }
}
