using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Kudos.GameLogic
{
    public static class SimulatedInput
    {
        static bool touchSupported => UnityEngine.Input.touchSupported;

        static SimulateTouchWithMouse simulator = new SimulateTouchWithMouse();

        static Touch? fakeTouch => simulator.FakeTouch;

        public static bool GetButton(string buttonName)
        {
            return UnityEngine.Input.GetButton(buttonName);
        }

        public static bool GetButtonDown(string buttonName)
        {
            return UnityEngine.Input.GetButtonDown(buttonName);
        }

        public static bool GetButtonUp(string buttonName)
        {
            return UnityEngine.Input.GetButtonUp(buttonName);
        }

        public static bool GetMouseButton(int button)
        {
            return UnityEngine.Input.GetMouseButton(button);
        }

        public static bool GetMouseButtonDown(int button)
        {
            return UnityEngine.Input.GetMouseButtonDown(button);
        }

        public static bool GetMouseButtonUp(int button)
        {
            return UnityEngine.Input.GetMouseButtonUp(button);
        }

        public static int touchCount
        {
            get
            {
                if (touchSupported)
                {
                    return UnityEngine.Input.touchCount;
                }
                else
                {
                    return fakeTouch.HasValue ? 1 : 0;
                }
            }
        }

        public static Touch GetTouch(int index)
        {
            if (touchSupported)
            {
                return UnityEngine.Input.GetTouch(index);
            }
            else
            {
                Assert.IsTrue(fakeTouch.HasValue && index == 0);
                return fakeTouch.Value;
            }
        }

        public static Touch[] touches
        {
            get
            {
                if (touchSupported)
                {
                    return UnityEngine.Input.touches;
                }
                else
                {
                    return fakeTouch.HasValue ? new[] { fakeTouch.Value } : new Touch[0];
                }
            }
        }
    }

    public class SimulateTouchWithMouse
    {
        private float lastUpdateTime;
        private Vector3 prevMousePos;
        private Touch? fakeTouch;

        public Touch? FakeTouch
        {
            get
            {
                Update();
                return fakeTouch;
            }
        }

        void Update()
        {
            if (Time.time != lastUpdateTime)
            {
                lastUpdateTime = Time.time;

                var curMousePos = UnityEngine.Input.mousePosition;
                var delta = curMousePos - prevMousePos;
                prevMousePos = curMousePos;

                fakeTouch = CreateTouch(GetPhase(delta), delta);
            }
        }

        static TouchPhase? GetPhase(Vector3 delta)
        {
            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                return TouchPhase.Began;
            }
            else if (UnityEngine.Input.GetMouseButton(0))
            {
                return delta.sqrMagnitude < 0.01f ? TouchPhase.Stationary : TouchPhase.Moved;
            }
            else if (UnityEngine.Input.GetMouseButtonUp(0))
            {
                return TouchPhase.Ended;
            }
            else
            {
                return null;
            }
        }

        static Touch? CreateTouch(TouchPhase? phase, Vector3 delta)
        {
            if (!phase.HasValue)
            {
                return null;
            }

            var curMousePos = Input.mousePosition;
            return new Touch
            {
                phase = phase.Value,
                type = TouchType.Indirect,
                position = curMousePos,
                rawPosition = curMousePos,
                fingerId = 0,
                tapCount = 1,
                deltaTime = Time.deltaTime,
                deltaPosition = delta
            };
        }

    }
}

