using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniRx;

public class LevelMovement : MonoBehaviour, IDisposable
{
    IDisposable moveDispose;

    [SerializeField]
    float speed = 1;

    public void ReInit()
    {
        transform.position = Vector3.zero;
        moveDispose?.Dispose();
        moveDispose = Observable.EveryFixedUpdate()
            .Subscribe(_=> 
            {
                transform.Translate(Vector3.left * speed * Time.fixedDeltaTime);
            });
    }

    public void Dispose()
    {
        transform.position = Vector3.zero;
        moveDispose?.Dispose();
    }
}
