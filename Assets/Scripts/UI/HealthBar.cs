using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Kudos.GameObjects;

namespace Kudos.UI
{
    public class HealthBar : MonoBehaviour
    {
        [Inject] Player player;
        private List<Image> heartImages;

        [SerializeField]
        private Image heartPrefab;

        private void Start()
        {
            heartImages = new List<Image>();
            InitHearts();
            player.OnInit += InitHearts;
            player.OnTakeDamage += () => 
            {
                UpdateHearts(player.CurrentHP);
            };
        }

        private void InitHearts()
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
            heartImages.Clear();
            for (int i = 0; i < player.StartHP; i++)
            {
                Image image = Instantiate(heartPrefab, transform);
                heartImages.Add(image);
            }

            UpdateHearts(player.StartHP);
        }

        public void UpdateHearts(int value)
        {
            value = Mathf.Clamp(value, 0, heartImages.Count);

            foreach (var heart in heartImages)
            {
                heart.color = new Color(0,0,0, 100);
            }

            for (int i = 0; i < value; i++)
            {
                heartImages[i].color = Color.white;
            }
        }
    }
}
