using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Kudos.GameObjects;
using Kudos.GameObjects.Bosses;
using Zenject;

public class TestBossProgressSlider : MonoBehaviour
{
    private Slider slider;
    [Inject] BossController boss;

    private void Awake()
    {
        slider = GetComponent<Slider>();
        slider.minValue = 0;
        slider.maxValue = boss.StartHP;
    }

    private void Update()
    {
        slider.value = boss.CurrentHP;
    }
}
