using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Kudos.GameObjects;
using Zenject;

public class TestProgressSlider : MonoBehaviour
{
    private Slider slider;
    private FinishCollider finishCollider;
    [Inject] Player player;

    Vector3 startPos;
    Vector3 finishPos;

    private void Awake()
    {
        slider = GetComponent<Slider>();
        finishCollider = FindObjectOfType<FinishCollider>();
        startPos = player.transform.position;
        finishPos = finishCollider.transform.position;
        slider.minValue = startPos.x;
        slider.maxValue = finishPos.x;
    }

    private void Update()
    {
        slider.value = player.transform.position.x;
    }
}
