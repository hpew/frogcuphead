using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Kudos.GameControllers;
using Kudos.GameObjects;
using UniRx;
using System;

public class EnemyCounter : MonoBehaviour, IDisposable
{
    [SerializeField]
    private Text text;

    [Inject] RunAndGunManager manager;
    [Inject] IEnemy[] enemies;

    int count;

    IDisposable dispose;

    private void Start()
    {
        manager.OnRebuild += ResetCounter;
        ResetCounter();

        dispose = MessageBroker.Default.Receive<GoalMessage>()
            .Subscribe(x =>
            {
                if (x._GoalType == GoalMessage.GoalType.enemies)
                {
                    AddCount(1);
                }
            });
    }

    private void ResetCounter()
    {
        count = 0;
        text.text = count.ToString() + "/" + enemies.Length;
    }

    public void AddCount(int value)
    {
        count += value;
        text.text = count.ToString() + "/" + enemies.Length;
    }

    public void Dispose()
    {
        dispose?.Dispose();
    }
}
