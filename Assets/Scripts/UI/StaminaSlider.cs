using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaSlider : MonoBehaviour
{
    private Slider _slider;
    public Slider Slider => _slider;

    private void Awake()
    {
        _slider = GetComponent<Slider>();
    }
}
