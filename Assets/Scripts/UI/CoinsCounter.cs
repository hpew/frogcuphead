using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Kudos.GameControllers;
using UniRx;

public class CoinsCounter : MonoBehaviour, IDisposable
{
    [SerializeField]
    private Text text;

    [Inject] RunAndGunManager manager;

    int count;

    IDisposable dispose;

    private void Start()
    {
        manager.OnRebuild += ResetCounter;
        ResetCounter();

        dispose = MessageBroker.Default.Receive<GoalMessage>()
            .Subscribe(x =>
            {
                if (x._GoalType == GoalMessage.GoalType.coins)
                {
                    AddCount(1);
                }
            });
    }

    private void ResetCounter()
    {
        count = 0;
        text.text = count.ToString();
    }

    public void AddCount(int value)
    {
        count += value;
        text.text = count.ToString();
    }

    public void Dispose()
    {
        dispose?.Dispose();
    }
}

public class GoalMessage
{
    public enum GoalType { coins, enemies }
    public GoalType _GoalType { get; private set; }
    public GoalMessage(GoalType goalType)
    {
        _GoalType = goalType;
    }

    public static GoalMessage Create(GoalType goalType)
    {
        return new GoalMessage(goalType);
    }
}
