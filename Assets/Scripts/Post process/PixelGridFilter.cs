using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelGridFilter : SimpleFilter
{
    [SerializeField]
    private int _xTileCount = 100;
    private int _yTileCount = 0;

    [SerializeField]
    private Texture _overlayTexture;

    [SerializeField]
    private Color _overlayColor = Color.white;

    protected override void Init()
    {
        //_mat.SetTexture("_OverlayTex", _overlayTexture);
        //_yTileCount = Mathf.RoundToInt((float)Screen.height / Screen.width * _xTileCount);
    }

    protected override void OnUpdate()
    {
        //_mat.SetColor("_OverlayColor", _overlayColor);
        //_mat.SetInt("_XTileCount", _xTileCount);

        //_mat.SetInt("_YTileCount", Mathf.RoundToInt((float)Screen.height / Screen.width * _xTileCount));
        
    }

    protected override void UseFilter(RenderTexture src, RenderTexture dst)
    {
        var temp = RenderTexture.GetTemporary(_xTileCount, Mathf.RoundToInt((float)src.height / src.width * _xTileCount));
        temp.filterMode = FilterMode.Point;
        Graphics.Blit(src, temp);
        //Graphics.Blit(temp, dst, _mat);
        Graphics.Blit(temp, dst);
        RenderTexture.ReleaseTemporary(temp);
    }
}
