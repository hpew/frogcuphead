%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: MaskBoss Avatar Mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Low_Mask
    m_Weight: 1
  - m_Path: Low_Mask/Balls_Mask
    m_Weight: 1
  - m_Path: Low_Mask/Ears_Mask
    m_Weight: 1
  - m_Path: Low_Mask/Eyes_Mask
    m_Weight: 1
  - m_Path: Low_Mask/Forehead_Mask
    m_Weight: 1
  - m_Path: Low_Mask/Hands2_Mask
    m_Weight: 1
  - m_Path: Low_Mask/Hands3_Mask
    m_Weight: 1
  - m_Path: Low_Mask/Hands_Mask
    m_Weight: 1
  - m_Path: Low_Mask/Mouth_Mask
    m_Weight: 1
  - m_Path: Low_Mask/Nose_Mask
    m_Weight: 1
  - m_Path: Main_joint
    m_Weight: 1
  - m_Path: Main_joint/Eye_L_joint
    m_Weight: 1
  - m_Path: Main_joint/Eye_R_joint
    m_Weight: 1
  - m_Path: Main_joint/Eyebrow_L1_jnt
    m_Weight: 1
  - m_Path: Main_joint/Eyebrow_L2_jnt
    m_Weight: 1
  - m_Path: Main_joint/Eyebrow_R1_jnt
    m_Weight: 1
  - m_Path: Main_joint/Eyebrow_R2_jnt
    m_Weight: 1
  - m_Path: Main_joint/Forehead_joint
    m_Weight: 1
  - m_Path: Main_joint/Hand_2L_jnt
    m_Weight: 0
  - m_Path: Main_joint/Hand_2R_jnt
    m_Weight: 0
  - m_Path: Main_joint/Hand_3L_jnt
    m_Weight: 0
  - m_Path: Main_joint/Hand_3R_jnt
    m_Weight: 0
  - m_Path: Main_joint/Hand_L_joint
    m_Weight: 0
  - m_Path: Main_joint/Hand_R_joint
    m_Weight: 0
  - m_Path: Main_joint/Mouth_Bottom_joint
    m_Weight: 1
  - m_Path: Main_joint/Mouth_Top_joint
    m_Weight: 1
