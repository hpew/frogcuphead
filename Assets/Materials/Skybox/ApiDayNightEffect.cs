using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace HPEW.ShaderAPI
{

    public class ApiDayNightEffect : MonoBehaviour
    {

        Material materialSkybox;
        public float speed = 0.1f;

        public float TimeDay
        {
            get
            {
                return materialSkybox.GetFloat("_LerpDayAndNight");
            }
            set
            {
                materialSkybox.SetFloat("_LerpDayAndNight", value % 4);
            }
        }

        private void Awake()
        {
            materialSkybox = RenderSettings.skybox;
        }

        private void Update()
        {
            TimeDay += Time.deltaTime * speed;
        }
    }
}
